//
//  PicklistState.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import UIKit

struct ConfigState {
    var availableStores: [Shop]?
    var selectedStore: Shop?
    var availableUsers: [User]?
    var selectedUser: User?
    var selectedLanguage: String?
}

