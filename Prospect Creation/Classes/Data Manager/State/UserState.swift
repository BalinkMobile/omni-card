//
//  UserState.swift
//  Prospect Creation
//
//  Created by elie buff on 08/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

struct UserState {
    var userInfo: [UserDefaultKey:Any]?
    var isFiltered: Bool?
    var isLoading: Bool?
}

