//
//  State.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import ReSwift

struct State: StateType {
    var userState: UserState
    var configState: ConfigState
    var prospectState: ProspectState
}
