//
//  ProspectState.swift
//  Prospect Creation
//
//  Created by elie buff on 10/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
import UIKit

struct ProspectState {
    var prospectFields: [String:(label: String, value: AnyObject)]?
    var imgSignature: UIImage?
    var isLoading: Bool?
    
    
    init(){
        prospectFields = [String:(label: String, value: AnyObject)]()
    }
}


