//
//  UserActions.swift
//  Prospect Creation
//
//  Created by elie buff on 08/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//


import ReSwift


struct UserInfoSetAction: Action {
    let userInfo: [UserDefaultKey:Any]?
}

struct UserInforUpdateValueAction: Action {
    let key: UserDefaultKey
    let value: Any
}

struct IsLoadingUserAction: Action {
    let isLoading: Bool?
}

