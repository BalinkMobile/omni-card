//
//  PicklistActions.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import ReSwift

struct SetAvailableStores: Action {
    let stores: [Shop]?
}

struct SetSelectedStore: Action {
    let selectedStore: Shop?
}

struct SetAvailableUsers: Action {
    let users: [User]?
    let selectedUser: User?
}

struct SetSelectedUser: Action {
    let selectedUser: User?
}

struct SetSelectedLanguage: Action {
    let selectedLanguage: String?
}

struct SetLoadSelectedValues: Action {
    let selectedLanguage: String?
    let selectedShop: Shop?
}
