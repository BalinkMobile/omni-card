//
//  ProspectActions.swift
//  Prospect Creation
//
//  Created by elie buff on 10/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import ReSwift


struct AddProspectFields: Action {
    let fields: [String:(label: String, value: AnyObject)]?
}

struct AddSignature: Action {
    let imageSignature: UIImage?
}

struct SetIsLoadingProspectCreation: Action {
    let isLoading: Bool?
}
