//
//  UserReducer.swift
//  Prospect Creation
//
//  Created by elie buff on 08/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import ReSwift

public class UserReducer{
    static func getReducer(state: UserState?, action: Action) -> UserState? {
        var state = state ?? UserState()
        
        switch action {
        case let action as UserInfoSetAction:
            state.userInfo = action.userInfo
        case let action as UserInforUpdateValueAction:
            state.userInfo?[action.key] = action.value
        case let action as IsLoadingUserAction:
            state.isLoading = action.isLoading
        default:
            break
        }
        
        return state
    }
}


