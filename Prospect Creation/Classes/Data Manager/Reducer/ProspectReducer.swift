//
//  ProspectReducer.swift
//  Prospect Creation
//
//  Created by elie buff on 10/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import ReSwift

public class ProspectReducer{
    static func getReducer(state: ProspectState?, action: Action) -> ProspectState? {
        var state = state ?? ProspectState()
        
        switch action {
        case let action as AddProspectFields:
            if let fields = action.fields{
                for (key, item) in fields{
                    state.prospectFields?[key] = item
                }
            } else {
                state.prospectFields = [String:(label: String, value: AnyObject)]()
            }
        case let action as AddSignature:
            state.imgSignature = action.imageSignature
        case let action as SetIsLoadingProspectCreation:
            state.isLoading = action.isLoading
         default:
            break
        }
        
        return state
    }
}

