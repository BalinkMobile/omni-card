//
//  AppReducer.swift
//  Clientleling Template
//
//  Created by elie buff on 05/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
import ReSwift

func appReducer(action: Action, state: State?) -> State {
    return State(
        userState: UserReducer.getReducer(state: state?.userState, action: action)!,
        configState: ConfigReducer.getReducer(state: state?.configState, action: action)!,
        prospectState: ProspectReducer.getReducer(state: state?.prospectState, action: action)!
    )
}



