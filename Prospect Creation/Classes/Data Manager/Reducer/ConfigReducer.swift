//
//  PicklistReducer.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import ReSwift

public class ConfigReducer{
    static func getReducer(state: ConfigState?, action: Action) -> ConfigState? {
        var state = state ?? ConfigState()
        
        switch action {
        case let action as SetAvailableStores:
            state.availableStores = action.stores
        case let action as SetSelectedStore:
            state.selectedStore = action.selectedStore
        case let action as SetAvailableUsers:
            state.availableUsers = action.users
            if let selectedUser = action.selectedUser{
                state.selectedUser = selectedUser
            }
        case let action as SetSelectedUser:
            state.selectedUser = action.selectedUser
        case let action as SetSelectedLanguage:
            state.selectedLanguage = action.selectedLanguage
        case let action as SetLoadSelectedValues:
            state.selectedLanguage = action.selectedLanguage
            state.selectedStore = action.selectedShop
        default:
            break
        }
        
        return state
    }
}
