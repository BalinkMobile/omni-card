//
//  ConfigurationUtils.swift
//  Berluti
//
//  Created by Jeremy Martiano on 15/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import UIKit
import CoreData
import PromiseKit
import ReSwift

var langValues =  [(label: "Francais", value: "fr"),
                   (label: "English", value: "en_Us")]

class ConfigurationUtils{
    
    
    static func RetrieveConfigValues(viewController :UIViewController?) -> Promise<Void> {
        let queryParams = ["picklists":"Salutation;InstantMessage__pc;Nationality__pc;BillingCountry__pc;Mobile1CountryCode__pc"]
        
        return WebService.ExecuteWS(restMethod: .GET, wsName: "/config", queryParams: queryParams, body: nil)
            .then { restResponse -> Promise<Void> in
                let records = restResponse.asJsonDictionary()
                if let picklists = records["picklists"] as? [[String : AnyObject]] {
                    PickListObject.Upsert(picklists,
                                          in: CoreDataUtils.sharedInstance.createChildContext(),
                                          deleteAllNotIn: true)
                }
                
                if let userInfos = records["userInfos"] as? [String : AnyObject] {
                    for (k, v) in userInfos{
                        if let userDefaultKey = UserDefaultKey(rawValue: k){
                            UserDefaultUtils.saveItem(key: userDefaultKey, value: v)
                        }
                    }
                    UserUtils.getLocalUserInfo(loadLanguage: true)
                }
                return Promise()
            }
    }
    //---------------------------------------Users actions--------------------------------------------
    static func RetrieveUsers(viewController :UIViewController?)  -> Promise<Void>{
        let storeId = mainStore.state.userState.userInfo?[.storeCode] as! String
        
        let queryParams = ["storeId" : storeId]
        return WebService.ExecuteWS(restMethod: .GET, wsName: "/user", queryParams: queryParams, body: nil)
            .then { restResponse -> Promise<Void> in
                let records = restResponse.asJsonArray()
                
                User.Upsert(records, in: CoreDataUtils.sharedInstance.createChildContext(), deleteAllNotIn: true)
                getLocalUsers()
                return Promise()
        }
    }
    
    static func getLocalUsers(selectFirstUser: Bool = true){
        let allUsers = User.getRecords(sortDescriptors: [(key:"firstName", ascending:true)])
        if let allUsers = allUsers as? [User]{
            let firstUser = (selectFirstUser && allUsers.count > 0) ? allUsers[0] : nil
            mainStore.dispatch(SetAvailableUsers(users: allUsers, selectedUser: firstUser))
        }
    }
    
    static func selectUser(user: User){
        mainStore.dispatch(SetSelectedUser(selectedUser: user))
    }
    
    //---------------------------------------Language actions--------------------------------------------
    static func selectLanguage(languageCode: String?){
        UserDefaultUtils.saveItem(key: .selectedLanguage, value: (languageCode ?? "fr") as Any)
        UserUtils.getLocalUserInfo(loadLanguage: true)
    }
    
    static func getSelectedPicklistValuesLabelsTupple(itemsSelected: [(label :String, value :String)]?)-> (labels:String, values:String)?{
        let selectedItems = itemsSelected
        
        return selectedItems?.reduce((labels:"", values:""), { (prev, currentSelectedValue) -> (labels:String, values:String) in
            let separator = prev.labels == "" ? "":";"
            
            return (
                labels: prev.labels + separator + currentSelectedValue.label,
                values: prev.values + separator + currentSelectedValue.value
            )
        })
    }
    
    static func getSelectedPicklistValuesLabelsArray(objectName: String,fieldName:String, selectedValues:String?, controllingFieldValue:String? = nil, onlyFromValues: Bool = false) -> [(label: String, value: String)]?{
        
        var values: [String] = []
        if let selectedValues = selectedValues{
            values = selectedValues.components(separatedBy:";")
        }
        
        var results = [(label: String, value: String)]()
        let picklistData = PickListObject.getValues(for: fieldName, in: objectName, with: controllingFieldValue)
        if let picklistData = picklistData{
            for item in picklistData{
                if values.count == 0 || values.contains(item.value.trimmingCharacters(in: .whitespaces)){
                    results.append((label: (onlyFromValues ? item.value.localized() : item.label), value: item.value))
                }
            }
        
            return results
        }
        return nil
    }
    
    static func changeLanguage(_ vc: UIViewController, sender : UIButton){
        vc.view.endEditing(true)
        let langPicker = PopPickerManager(srcButton: sender)
        let selectedLang = mainStore.state.userState.userInfo?[.selectedLanguage] as? String
        let selectedValue = langValues.first(where: {$0.value == selectedLang ?? "fr"})
        langPicker.popTable(vc, width: CGFloat(300), popOverDir: .up, values: langValues, selectedValue: selectedValue
            , dataChanged:{ (label, value) -> () in
                ConfigurationUtils.selectLanguage(languageCode: value as? String ?? "fr")
        })
    }
    
    
    /*
    

    
    static func getPicklistLabels(objectName:String, fieldName:String, values:String?, controllingFieldValue:String? = nil)-> String{
        var labels = ""
        if let values = values{
            let valuesArray = values.components(separatedBy: ";")
            let picklistData = PickListObject.getValues(for: fieldName, in: objectName, with: controllingFieldValue)
            if let picklistData = picklistData{
                labels = picklistData.reduce("", { (prev, item) in
                    var result = prev
                    if valuesArray.contains(item.value.trimmingCharacters(in: .whitespaces)){
                        result = (result == "" ? "" : result + ";") + item.label
                    }
                    return result
                })
            }
        }
        
        return labels
    }
    

    
    static func setCurrentPicklist(currentPicklist: (object: String, field: String, value:Any?,isLookup:Bool)?, selectedPair: (labels: String, values: String)?){
        mainStore.dispatch(ConfigSetCurrentObject(currentObject: currentPicklist))
        if let currentPicklist = currentPicklist{
            var values = PickListObject.getValues(for: currentPicklist.field, in: (currentPicklist.object), with: nil) ?? [(label :String, value :String)]()
            if currentPicklist.isLookup{
                let pairsArray = selectedPair != nil ? convertToArray(pair:selectedPair!) : nil
                    for pair in (pairsArray ?? [(label: String, value: String)]()){
                        if !(values.contains(where: {$0.label == pair.label}) ){
                            values.append(pair)
                        }
                    }
                selectPicklists(picklistValues: pairsArray)
            } else{
                selectPicklists(picklistValues: getSelectedPicklistValuesLabelsArray(objectName: currentPicklist.object, fieldName: currentPicklist.field, selectedValues:selectedPair?.values))
            }
            
            mainStore.dispatch(ConfigSetItemsAction(items: values))
        }
    }
    
    static func convertToArray(pair: (labels: String, values: String))->[(label: String, value: String)]{
        let valuesArray = pair.values.components(separatedBy: ";")
        let labelsArray = pair.labels.components(separatedBy: ";")
        
        return valuesArray.enumerated().map{(index, value) in
            return (label: labelsArray[index], value: value)
        }
    }
    
    static func getLookupOptions(by searchText:String, objectName:String) -> Promise<Void>
    {
        mainStore.dispatch(IsLoadingConfigAction(isLoading: true))
        let query = SOQLQueries.getLookupOptionsQuery(searchText: searchText,objectName: objectName)
        return WebService.SOQLExecuter(query: query)
            .done{ (records) in
                    var picklistValues = mainStore.state.configState.items ?? [(label :String, value :String)]()
                    for record in records{
                        if let Id = record["Id"] as? String, let Label = record["Name"] as? String{
                            if !picklistValues.contains(where: {$0.label == Label}){
                                picklistValues.append((label: Label, value: Id))
                            }
                        }
                    }
                    mainStore.dispatch(ConfigSetItemsAction(items: picklistValues))
            }.ensure {
                mainStore.dispatch(IsLoadingConfigAction(isLoading: false))
        }
    }
    
    static func setCurrentFieldData (currentData: (object: String,field: String,value: Any?,isLookup:Bool)){
        var pair: (labels: String, values: String)? = nil
        if let value = currentData.value as? (label: String?, value: String?){
            pair = value.value != nil ? (labels: value.label ?? "" , values: value.value ?? "") : nil
        }
        else{
            pair = ((labels : getPicklistLabels(objectName: currentData.object, fieldName: currentData.field, values: currentData.value as? String), values : currentData.value as? String ?? "") )
        }
        
        ConfigurationUtils.setCurrentPicklist(currentPicklist: currentData, selectedPair: pair)
    }
    */
    
}
