//
//  ProspectUtils.swift
//  Prospect Creation
//
//  Created by elie buff on 08/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//


import ReSwift
import UIKit
import SalesforceSDKCore
import PromiseKit

class ProspectUtils{
    
    static func updateProspectField(fields : [String: (label: String, value: AnyObject)]?){
        mainStore.dispatch(AddProspectFields(fields: fields))
    }
    
    static func addProspectSignature(signatureImage: UIImage?){
        mainStore.dispatch(AddSignature(imageSignature: signatureImage))
    }
    
    static func updateClientDetails() ->Promise<Void>{
        let storeId = mainStore.state.userState.userInfo?[.storeId] as! String
        let userId = mainStore.state.configState.selectedUser?.id ?? ""
        
        let prospectState =  mainStore.state.prospectState
        var queryParams = [String: AnyObject]()
        
        
        if let prospectFields = prospectState.prospectFields{
            for (key, item) in prospectFields{
                if key == "phone1CountryCode" && prospectFields["phone"] == nil{
                    continue
                }
                if key == "mobile1CountryCode" && prospectFields["mobile"] == nil{
                    continue
                }
                queryParams[key] = item.value
            }
        }
        
        if let imageSignature = prospectState.imgSignature, let imageData = UIImageJPEGRepresentation(imageSignature, 0.5){
            queryParams["imageSignature"] = imageData.base64EncodedString() as AnyObject
        }
        
         queryParams["storeId"] = storeId as AnyObject
         queryParams["declaredSA"] = userId as AnyObject
        
        
        
        mainStore.dispatch(SetIsLoadingProspectCreation(isLoading: true))
        return WSClient.upsertMaps(mapObjects: [queryParams])
            .then { restResponse -> Promise<Void> in
                return Promise()
            }.ensure {
                mainStore.dispatch(SetIsLoadingProspectCreation(isLoading: false))
            }
    }
}

