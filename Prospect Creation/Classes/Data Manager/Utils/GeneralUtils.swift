//
//  File.swift
//  Berluti
//
//  Created by elie buff on 07/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//
import Foundation
import os
import PromiseKit

class GeneralUtils{
    static var partialDeleteExcludeObj = [""]
    static var newStoreDeleteExcludeObj = [""]
    static var newVersionDeleteExcludeObj = [""]
    
    static func postLogin(){
        os_log("Post Login", log: OSLog.default, type: .info)
        
        if (UserDefaultUtils.getItem(key: .lastLogin) as? Date == nil){
            UserDefaultUtils.saveItem(key: .lastLogin, value: Date())
        }
        
        UserUtils.getLocalUserInfo(loadLanguage: true)
        checkDailyUpdate()
    }
    
    static func cleanDB(clearCacheType : ClearCacheType){
       /* var excludeObj = [String]()
        
        if clearCacheType == ClearCacheType.Logout{
            excludeObj = partialDeleteExcludeObj
        }
        if clearCacheType == ClearCacheType.NewStore{
            excludeObj = newStoreDeleteExcludeObj
            UserDefaultUtils.removeItems(keys: [.lastClientUpdate])
        }
        else if clearCacheType == ClearCacheType.Partial{
            excludeObj = partialDeleteExcludeObj
        }
        else if clearCacheType == ClearCacheType.NewVersion{
            excludeObj = newVersionDeleteExcludeObj
        }
        else if clearCacheType == ClearCacheType.Complete{
             UserDefaultUtils.removeItems(keys: [.lastClientUpdate])
        }
        CoreDataUtils.sharedInstance.deleteAllData(excludeObj: excludeObj)
        
        UserUtils.removeUserInfo()
        UserDefaultUtils.removeItems(keys: [.lastUpdate, .lastLogin])*/
    }
    
    static func ClearCache(clearCacheType:ClearCacheType){
        cleanDB(clearCacheType: clearCacheType)
        GeneralUtils.postLogin()
    }
    
    static private func checkDailyUpdate()
    {
        var forceUpdate = false
        if let isDev = Utils.getPlistValue(for: "IsDev") as? Bool{
            forceUpdate = isDev   
        }
        
        guard let lastUpdate =  UserDefaultUtils.getItem(key: .lastUpdate) as? Date else{
            performDailyUpdate()
            return
        }
        
        if forceUpdate || !NSCalendar.current.isDateInToday(lastUpdate){
            performDailyUpdate()
        }
    }
    
    static private func performDailyUpdate(){
        ConfigurationUtils.RetrieveConfigValues(viewController: nil)
            .then{_ -> Promise<Void> in
                let getUsersPromise  = ConfigurationUtils.RetrieveUsers(viewController: nil)
                
                let promises = [getUsersPromise]
                return when(fulfilled: promises)
            }.done{_ in
                UserDefaultUtils.saveItem(key: .lastUpdate, value: Date())
        }
    }
        
}
