//
//  UserUtils.swift
//  Prospect Creation
//
//  Created by elie buff on 08/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//


import ReSwift
import SalesforceSDKCore
import PromiseKit

class UserUtils{
    
    
    static let userDescriptionFields = [UserDefaultKey.firstName,
                                        UserDefaultKey.lastName,
                                        UserDefaultKey.storeCode,
                                        UserDefaultKey.storeId,
                                        UserDefaultKey.fullPhotoUrl,
                                        UserDefaultKey.userLanguage,
                                        UserDefaultKey.email,
                                        UserDefaultKey.name]
    
    static func getLocalUserInfo(loadLanguage: Bool){
        if UserDefaultUtils.getItem(key: .selectedLanguage) == nil{
            UserDefaultUtils.saveItem(key: .selectedLanguage, value: "fr")
        }
        let userInfo = UserDefaultUtils.getItems(keys: UserDefaultKey.allValues)
        mainStore.dispatch(UserInfoSetAction(userInfo: userInfo))
        
        if loadLanguage{
            LocalizeUtils.setupLocalizedBundle(salesForceLang: (userInfo[.selectedLanguage] as? String) ?? (userInfo[.userLanguage] as? String) ??  "fr")
        }
    }
    
    static func removeUserInfo()
    {
        UserDefaultUtils.removeItems(keys: [UserDefaultKey.firstName,
                                            UserDefaultKey.lastName,
                                            UserDefaultKey.fullPhotoUrl,
                                            UserDefaultKey.userLanguage])
        mainStore.dispatch(UserInfoSetAction(userInfo: nil ))
    }
    
    static func updateUser(viewController: UIViewController?, profilInfos: [UserDefaultKey: Any],completionHandler: (() -> ())? = nil){
        
        func getAndDispatch(){
            for (key,value) in profilInfos{
                UserDefaultUtils.saveItem(key: key, value: value)
            }
            getLocalUserInfo(loadLanguage: false)
            GeneralUtils.ClearCache(clearCacheType:ClearCacheType.NewStore)
        }
        
        var bodyParams = [String:AnyObject]()
        for (key,value) in profilInfos{
            bodyParams[key.rawValue] = value as AnyObject
        }
        
        mainStore.dispatch(IsLoadingUserAction(isLoading: true))
    }
}

