//
//  DatesSliderCell.swift
//  Clientleling Template
//
//  Created by Nehora Sharabi on 13/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class DatesSliderCell: UICollectionViewCell {
    @IBOutlet weak var weekDayLbl: UILabel!
    @IBOutlet weak var dayLbl: UILabel!
    @IBOutlet weak var selectedCellBackground: UIView!
    @IBOutlet weak var hasItemsSignView: RoundedView!
    
    let regularDateColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
    let selectedDateColor = #colorLiteral(red: 0.2274509804, green: 0.4, blue: 0.4156862745, alpha: 1)
    let pastDateColor = #colorLiteral(red: 0.662745098, green: 0.6588235294, blue: 0.6509803922, alpha: 1)
    
    var date: Date = Date() {
        didSet {
            weekDayLbl.text = date.toString(format: .weekDay)
            dayLbl.text = date.toString(format: .day)
            if date.todayOrFutur(){
                weekDayLbl.textColor = regularDateColor
                hasItemsSignView.backgroundColor = regularDateColor
            } else {
                weekDayLbl.textColor = pastDateColor
                hasItemsSignView.backgroundColor = pastDateColor
            }
        }
    }
    
    var isSelectedCell: Bool = false {
        didSet {
            selectedCellBackground.alpha = isSelectedCell ? 1 : 0
            if isSelectedCell {
                dayLbl.textColor = selectedDateColor
            } else {
                dayLbl.textColor = date.todayOrFutur() ? regularDateColor : pastDateColor
            }
        }
    }
    
    var isHasItems: Bool = false {
        didSet {
            hasItemsSignView.isHidden = !isHasItems
        }
    }
    
}
