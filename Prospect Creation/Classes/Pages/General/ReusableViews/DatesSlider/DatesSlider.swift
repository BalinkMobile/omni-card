//
//  DatesSlider.swift
//  Clientleling Template
//
//  Created by Nehora Sharabi on 06/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

protocol WeekdayPickerDelegate{
    func dayChanged(date:Date)
}

class DatesSlider: UIView{
    let DAYS_IN_PAST = 30
    let DAYS_IN_FUTUR = 30
    
    @IBOutlet weak var collection: UICollectionView!
    @IBOutlet weak var monthLbl: UILabel!
    
    var delegate: WeekdayPickerDelegate?
    var contentView : UIView?
    let today = Date().startOfDay()
    let weekdayIndex = Date().getWeekday() - 1
    let itemWidth = Int(UIScreen.main.bounds.width / 8)
    
    var selectedCellDate = Date().startOfDay()
    var firstScroll = false
    
    var eventDates:[Date]?{
        didSet{
            collection.reloadData()
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        initSlider()
        
        return view
    }
    
    func initSlider(){
        let cellNib = UINib(nibName: "DatesSliderCell", bundle: nil)
        collection.register(cellNib, forCellWithReuseIdentifier: "datesSliderCell")
    }
    
}

extension DatesSlider : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DAYS_IN_PAST + DAYS_IN_FUTUR
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collection.dequeueReusableCell(withReuseIdentifier: "datesSliderCell", for: indexPath) as! DatesSliderCell
        
        let cellDate = today.addDays(numOfDay: indexPath.row - DAYS_IN_PAST)
        cell.date = cellDate
        cell.isSelectedCell = cellDate == selectedCellDate
        cell.isHasItems = (eventDates?.contains(cellDate)) ?? false
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let height = 75
        return CGSize(width: itemWidth, height: height)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        selectedCellDate = today.addDays(numOfDay: indexPath.row - DAYS_IN_PAST)
        
        delegate?.dayChanged(date: selectedCellDate)
        self.collection.reloadData()
    }
    
    
    internal func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !firstScroll {
            let scrollOffset = (itemWidth * (DAYS_IN_PAST - 1 - weekdayIndex)) + itemWidth/2
            self.collection.scrollLeft(by: scrollOffset, animated: false)
            firstScroll = true
        }
        initMonthLabel()
    }
    
    func initMonthLabel() {
        guard !collection.indexPathsForVisibleItems.isEmpty else {
            self.monthLbl.text = ""
            return
        }
    
        let firstVisibleIndex = collection.indexPathsForVisibleItems.reduce(Int.max, { min($0, $1.row) })
        let lastVisibleIndex = collection.indexPathsForVisibleItems.reduce(Int.min, { max($0, $1.row) })
    
        let firstIndexMonth = today.addDays(numOfDay: (firstVisibleIndex - DAYS_IN_PAST)).toString(format: .monthYear)
        let lastIndexMonth = today.addDays(numOfDay: (lastVisibleIndex - DAYS_IN_PAST)).toString(format: .monthYear)
    
        let currentScrollMonth = firstIndexMonth == lastIndexMonth ? firstIndexMonth : "\(firstIndexMonth) - \(lastIndexMonth)"
        self.monthLbl.text = currentScrollMonth
    }
}

extension DatesSlider : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
