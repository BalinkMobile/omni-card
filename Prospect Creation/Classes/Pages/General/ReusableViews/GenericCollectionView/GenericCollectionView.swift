//
//  CategoryCollectionView.swift
//  Clientleling Template
//
//  Created by Shlomo Ariel on 23/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class GenericCollectionView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {
    
    @IBOutlet var collection: UICollectionView!
    
    var contentView : UIView?
    var itemList: [CellConfigurator] = [CellConfigurator]()
    
    var cellIdentifiers:[String]?{
        didSet{
            if let cellIdentifiers = cellIdentifiers{
                for cellIdentifier in cellIdentifiers{
                    collection.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
                }
            }
            
            if let flowLayout = collection.collectionViewLayout as? UICollectionViewFlowLayout{
                flowLayout.estimatedItemSize = CGSize(width: 1, height: 1)
            }
        }
    }
    
    func reloadData(){
        self.collection.reloadData()
        for (index, currentItem) in itemList.enumerated(){
            if currentItem.isSelectedCell{
                collection.selectIndex(indexPath: IndexPath(row: index, section: 0))
            }
        }
    }
    
    
    var doCallback:((GenericCollectionView, Int)->Void)?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.itemList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let item = itemList[indexPath.row]
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: type(of: item).reuseId, for: indexPath)
        item.configure(cell: cell)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        itemList[indexPath.row].isSelectedCell = true
        doCallback!(self, indexPath.row)
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        itemList[indexPath.row].isSelectedCell = false
        doCallback!(self, indexPath.row)
    }
}

extension GenericCollectionView : UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        return UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
}
