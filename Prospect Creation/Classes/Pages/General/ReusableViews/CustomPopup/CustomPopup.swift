//
//  Popup.swift
//  Berluti
//
//  Created by Jeremy Martiano on 18/12/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

class CustomPopup: UIView {
    var contentView : UIView?
    
    @IBOutlet weak var subtitle: UILabel!
    @IBOutlet weak var mainText: UILabel!
    @IBOutlet weak var icone: UIImageView!
    @IBOutlet weak var buttonsSV: UIStackView!
    
    var onComplete: ((_ tag: Int)->())?

    @objc func onClose(sender:UIButton){
        if let onComplete = onComplete{
            onComplete(sender.tag)
        }
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 0
        }) { (true) in
            self.removeFromSuperview()
        }
    }
    
    init(subject:String?, subtitle:String?,image:UIImage?,buttonsArray: [(title:String,textColor: UIColor)]?,onComplete:((Int)->())?){
        guard let app = UIApplication.shared.delegate as? AppDelegate, let window = app.window else{
            super.init(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
            return
        }
        super.init(frame:CGRect(x: window.frame.origin.x, y: window.frame.origin.y, width: window.frame.width, height: window.frame.height))
        xibSetup()
        
        
        if let iconeImg = image{
            self.icone.image = iconeImg
        }
        if let subject = subject{
            self.mainText.text = subject
        }
        if let subtitle = subtitle{
            self.subtitle.text = subtitle
        }
        
        self.onComplete = onComplete
        
        if let buttonsArray = buttonsArray{
            for (index,item) in buttonsArray.enumerated(){
                let button = UIButton()
                button.setTitle(item.title, for: .normal)
                button.setTitleColor(item.textColor, for: .normal)
                button.titleLabel?.font = UIFont.systemFont(ofSize: 17, weight: .regular)
                button.translatesAutoresizingMaskIntoConstraints = false
                button.tag = index
                button.addTarget(self, action: #selector(onClose(sender:)), for: .touchUpInside)
                self.buttonsSV.addArrangedSubview(button)
            }
        }
        
        self.alpha = 0
        self.isHidden = false
        window.addSubview(self)
        UIView.animate(withDuration: 0.3, animations: {
            self.alpha = 1
        })
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
}
