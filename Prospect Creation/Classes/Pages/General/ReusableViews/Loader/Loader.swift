//
//  Loader.swift
//  ICON
//
//  Created by elie buff on 30/03/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit
import FLAnimatedImage

class Loader: UIView {
    var contentView : UIView?
    
    @IBOutlet weak var loaderText: UILabel!
    @IBOutlet weak var loader: FLAnimatedImageView!
    
    var alphaValue:CGFloat?
    
    var textDisplayed: String? {
        didSet {
            if let textDisplayed = textDisplayed{
                loaderText.text = textDisplayed
            }
        }
    }
    
    var isLoading: Bool?{
        didSet{
            self.superview?.endEditing(true)
            if let isLoading = isLoading{
                if isLoading {
                    self.fadeIn(alphaValue ?? 1.0)
                } else {
                    self.fadeOut()
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func hide(){
        self.alpha = 0.0
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        let FLAimage:FLAnimatedImage = FLAnimatedImage(animatedGIFData: Utils.getData(name: "loader"))
        loader.animatedImage = FLAimage                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          
        return view
    }

}
