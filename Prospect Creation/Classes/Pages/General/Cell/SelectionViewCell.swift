//
//  SelectionViewCell.swift
//  Berluti
//
//  Created by Jeremy Martiano on 24/12/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

class SelectionViewCell: UITableViewCell {
    
    @IBOutlet weak var itemLabel: UILabel!
    var itemValue: String?
    
    var item: (label :String, value :String)? {
        didSet {
            if let item = item{
                itemLabel.text = item.label
                itemValue = item.value
            }
        }
    }
    
    var labelItem: String? {
        didSet {
            itemLabel.text = labelItem
        }
    }
}
