//
//  SelectionListViewController.swift
//  Berluti
//
//  Created by Jeremy Martiano on 24/12/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit
import ReSwift

protocol PicklistDelegate {
    func dataSaved(data: [AnyObject])
}

class PickListSelectionVC: UIViewController,UISearchBarDelegate{
    var IS_MULTISELECTION = false
    var IS_DISPLAY_NONE_CELL = true
    
    var items:[(label :String, value :String)]?
    var filteredItems = [(label :String, value :String)]()
    var selectedItems = [(label :String, value :String)]()
    var objectName: String?
    var loaderView: Loader?
    var customDelegateForDataReturn : PicklistDelegate?
    
    @IBOutlet weak var itemsTableView: UITableView!
    @IBOutlet weak var doneButtonItem: UIBarButtonItem!
    @IBOutlet weak var SearchButtonView: UIView!
    @IBOutlet weak var searchButtonViewHeight: NSLayoutConstraint!
    @IBOutlet weak var searchBar: UISearchBar!

    
    
    override func viewDidLoad(){
        super.viewDidLoad()
        
        itemsTableView.tableFooterView = UIView()
        definesPresentationContext = true
        
        if !IS_MULTISELECTION{
            doneButtonItem.isEnabled = false
            doneButtonItem.tintColor = UIColor.clear
        }
    }
    
    override func viewDidAppear(_ animated: Bool){
        setPageLoader()
    }
    
    func setPageLoader() {
        loaderView = Loader(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        loaderView?.textDisplayed = "Loading.."
        loaderView?.alpha = 0
        self.view.addSubview(loaderView!)
    }
    
    //UISearchBarDelegate functions
    func filterContentForSearchText(){
        
        let searchText = searchBar.text!
        filteredItems = items!.filter { item in
            if searchText == ""{
                return true
            }
            return (item.label.lowercased().contains(searchText.lowercased()))
        }
        itemsTableView.reloadData()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        searchBar.setShowsCancelButton(true, animated: true)
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if items?.count ?? 0 > 0{
            filterContentForSearchText()
        }
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        searchBar.setShowsCancelButton(false, animated: true)
    }
    
    // UIButtonBarItem functions
    @IBAction func onSearch(_ sender: Any) {
       /* if let searchText = self.searchBar.text{
                        ConfigurationUtils.getLookupOptions(by: searchText, objectName: objectName!)
        }*/
    }
    
    @IBAction func doneClicked(_ sender: Any) {
        customDelegateForDataReturn?.dataSaved(data: selectedItems as [AnyObject])
        dismissView()
    }
    
    @IBAction func cancelClicked(_ sender: Any){
        dismissView()
    }
    
    func dismissView(){
        _ = self.navigationController?.popViewController(animated: true)
    }
}


extension PickListSelectionVC: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int{
        if IS_MULTISELECTION || !IS_DISPLAY_NONE_CELL{
            return 1
        }
        return 2
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if IS_MULTISELECTION || !IS_DISPLAY_NONE_CELL{
            return filteredItems.count
        }
        return section == 0 ? 1 :filteredItems.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && !IS_MULTISELECTION && IS_DISPLAY_NONE_CELL{
            let cell = tableView.dequeueReusableCell(withIdentifier: "noneSelectionViewCell", for: indexPath)
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "selectionViewCell") as! SelectionViewCell
            
            cell.item = filteredItems[indexPath.row]
            let itemValue = filteredItems[indexPath.row]
            let selectedValue = (label: itemValue.label, value: itemValue.value)
            
            if selectedItems.contains(where: {$0 == selectedValue}){
                cell.accessoryType = UITableViewCellAccessoryType.checkmark
            }else{
                cell.accessoryType = UITableViewCellAccessoryType.none
            }
            
            return cell
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        if indexPath.section == 0 && !IS_MULTISELECTION && IS_DISPLAY_NONE_CELL{
            customDelegateForDataReturn?.dataSaved(data: [(label: "", value: "")] as [AnyObject])
            dismissView()
        }else{
            if !IS_MULTISELECTION{
                singleValuePicklistSelection(indexPath: indexPath)
            }else{
                let cell:SelectionViewCell = tableView.cellForRow(at: indexPath) as! SelectionViewCell
                multiValuePicklistSelection(cell: cell, indexPath: indexPath)
            }
        }
    }
    
    func singleValuePicklistSelection(indexPath: IndexPath){
        let itemValue = filteredItems[indexPath.row]
        customDelegateForDataReturn?.dataSaved(data: [(label: itemValue.label, value: itemValue.value)] as [AnyObject])
        dismissView()
    }
    
    func multiValuePicklistSelection(cell :SelectionViewCell, indexPath: IndexPath){
        let itemValue = filteredItems[indexPath.row]
        let selectedValue = (label: itemValue.label, value: itemValue.value)
        
        if selectedItems.contains(where: {$0 == selectedValue}){
            if let index = selectedItems.index(where: {$0 == selectedValue}){
                selectedItems.remove(at: index)
            }
            cell.accessoryType = UITableViewCellAccessoryType.none
        }else{
            selectedItems.append(selectedValue)
            cell.accessoryType = UITableViewCellAccessoryType.checkmark
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 60
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return 30
    }
}


extension PickListSelectionVC: StoreSubscriber{
    override func viewWillAppear(_ animated: Bool) {
        mainStore.subscribe(self){ subcription in
            subcription.select { state in state.configState }
        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool){
       // mainStore.unsubscribe(self)
       // mainStore.dispatch(ConfigSetItemsAction(items: nil))
    }
    
    
    func newState(state: ConfigState){
        /*
        if let selectedValues = state.selectedItems{
            self.selectedItems = selectedValues
        }
        
        if let _ = state.items{
            var items = state.items!
            for selectedItem in self.selectedItems{
                if !items.contains(where: {$0 == selectedItem}){
                    items.append(selectedItem)
                }
            }
            self.items = items.sorted{$0.label < $1.label}
            filterContentForSearchText()
        }
        
        if let currentObject = state.currentObject{
            self.objectName = currentObject.object
            if !currentObject.isLookup {
                searchButtonViewHeight.constant = 0.0
                SearchButtonView.isHidden = true
            } else {
                searchButtonViewHeight.constant = 80.0
                SearchButtonView.isHidden = false
            }
        }
        
        if let isLoading = state.isLoading, isLoading != loaderView?.isLoading{
            loaderView?.isLoading = isLoading
        }*/
    }
    
    
}

