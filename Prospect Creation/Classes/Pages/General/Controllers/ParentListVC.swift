//
//  BaseListVC.swift
//  IOS Template
//
//  Created by Jeremy Martiano on 03/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//


import UIKit

class ParentListVC: UIViewController{
    var NO_DATA_TITLE: String { get { return "No data".localized()}}
    var NO_DATA_SUBTITLE : String { get { return "Looks like you don't have data".localized()}}
    var NO_DATA_BUTTON_TITLE : String { get { return "Create Data".localized()}}
    var SET_EMPTYLIST_BUTTON :Bool { get { return false}}
    var NO_RESULTS_SEARCH :String { get { return "No Results".localized()}}
    var SEARCH_PLACEHOLDER :String { get { return"Search".localized()}}
    var LOADING_PLACEHOLDER :String { get { return "Loading Data...".localized()}}
    var RIGHT_BUTTON_TITLE :String { get { return "Select".localized()}}
    var LEFT_BUTTON_TITLE :String { get { return "Cancel".localized()}}
    
    var ADD_TITLE_BUTTONS:Bool { get { return false}}
    var ADD_SEARCHBAR:Bool { get { return false}}
    var IS_LARGE_TITLE:Bool { get { return true}}
    var IS_REFRESHABLE_LIST:Bool { get { return true}}
    var IS_MULTIPLE_SECTIONS:Bool { get { return true}}
    var DISPLAY_ANIMATED_MENU:Bool { get { return false}}
    var DISPLAY_INDEX_TITLE:Bool { get { return true}}
    
    var parentView: UIView!
    var tableView = UITableView()
    var refreshControl = UIRefreshControl()
    var animatedMenu : AnimatedSegmentedControl!
    var emptyListView: EmptyListView!
    var loaderView: Loader!
    let search = UISearchController(searchResultsController: nil)
    var isActionMode = false
    var cancelButton: UIBarButtonItem!
    var originalBackbutton: UIBarButtonItem!
    
    
    var filteredData = [Any]()
    var allData : [AnyObject]?{
        didSet{
            self.filteredData = allData ?? [AnyObject]()
        }
    }
    
    var sectionsMap = [String:[AnyObject]]()
    var headersArray = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        setupConstraints()
    }
    
    func initTable(_ containerView: UIView? = nil){
        if let _ = containerView{
            parentView = containerView!
            
        } else{
            parentView = self.view
        }
        
        if DISPLAY_ANIMATED_MENU{
            animatedMenu = AnimatedSegmentedControl()
            parentView.addSubview(self.animatedMenu)
            animatedMenu.translatesAutoresizingMaskIntoConstraints = false
            animatedMenu.segmentedValueChanged = animatedMenuValueChange
        }
        
        
        parentView.addSubview(self.tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
        
        if IS_LARGE_TITLE {
            self.setLargeTitle()
        }
        
        if ADD_SEARCHBAR {
            self.setSearchBar()
        }
        
        if ADD_TITLE_BUTTONS{
            self.setBarButtons()
        }
        
        self.setTableView()
        self.setPageLoader()
        self.setEmptyList()
    }
    
    
    func setTableView(){
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.tableFooterView = UIView()
        
    }
    
    func setLargeTitle(){
        self.navigationController?.navigationBar.prefersLargeTitles = true
        self.navigationController?.navigationItem.largeTitleDisplayMode = .automatic
    }
    
    func setSearchBar(){
        self.search.searchResultsUpdater = self
        self.search.obscuresBackgroundDuringPresentation = false
        self.navigationItem.searchController = search
        self.navigationItem.hidesSearchBarWhenScrolling = false
        self.search.searchBar.placeholder = SEARCH_PLACEHOLDER
        self.search.searchBar.delegate = self
        definesPresentationContext = true
    }
    
    func setBarButtons(){
        let rightButton = UIBarButtonItem(title: RIGHT_BUTTON_TITLE, style: .plain, target: self, action: #selector(rightBarButtonItemClick))
        self.navigationItem.rightBarButtonItem = rightButton
        originalBackbutton = self.navigationItem.leftBarButtonItem
        cancelButton = UIBarButtonItem(title: LEFT_BUTTON_TITLE, style: .plain, target: self, action: #selector(cancelSelectMode))
    }
    
    func toggleSelectMode () {
        self.search.searchBar.showsScopeBar = !self.isActionMode
        self.isActionMode = !self.isActionMode
        self.navigationItem.rightBarButtonItem?.title = self.isActionMode ? "Action".localized(): "Select".localized()
        self.navigationItem.leftBarButtonItem = self.isActionMode ? cancelButton : originalBackbutton
        self.search.searchBar.selectedScopeButtonIndex = 0
        self.filterContentForSearchText("")
        self.tableView.reloadData()
    }
    
    func setEmptyList() {
        emptyListView = EmptyListView()
        emptyListView.isHidden = true
        emptyListView.translatesAutoresizingMaskIntoConstraints = false
        emptyListView.isUserInteractionEnabled = false
        self.parentView.addSubview(emptyListView!)
    }
    
    func setPageLoader() {
        loaderView = Loader()
        loaderView.translatesAutoresizingMaskIntoConstraints = false
        loaderView?.textDisplayed = LOADING_PLACEHOLDER
        loaderView.hide()
        loaderView.alphaValue = 0.8
        self.parentView.addSubview(loaderView!)
    }
    
    func animatedMenuValueChange(index : Int){fatalError("Must Override")}
    
    func reloadData(){
        self.emptyListView.isHidden = filteredData.count > 0
        self.tableView.reloadData()
        setEmptyListViewText()
    }
    
    func setEmptyListViewText(){
        if let data = self.allData, data.count > 0{
            self.emptyListView.title.text = NO_RESULTS_SEARCH
            self.emptyListView.subTitle.text = ""
            self.emptyListView.createBtn.isHidden = true
        } else{
            self.emptyListView.title.text = NO_DATA_TITLE
            self.emptyListView.subTitle.text = NO_DATA_SUBTITLE
            self.emptyListView.createBtn.titleLabel?.text = NO_DATA_BUTTON_TITLE
            self.emptyListView.createBtn.isHidden = !SET_EMPTYLIST_BUTTON
            self.search.searchBar.showsScopeBar = false
        }
    }
    
    func initRefreshControl(){
        self.tableView.refreshControl = self.refreshControl
        self.refreshControl.addTarget(self, action: #selector(self.refreshList), for: UIControlEvents.valueChanged)
    }
    
    func isLoading(_ isLoading: Bool){
        UIView.animate(withDuration: 1.5, animations: {
            self.loaderView.alpha = isLoading ? 0.7 : 0
        })
        if self.refreshControl.isRefreshing == true{
            if isLoading == false{
                self.refreshControl.endRefreshing()
            }
        }else {
            loaderView?.isHidden = !isLoading
        }
    }
    
    @objc func cancelSelectMode() {
        toggleSelectMode()
    }
    
    @IBAction func rightBarButtonItemClick(_ sender: Any) {
        if isActionMode{
            actionModeFunc(sender)
            
        } else{
            toggleSelectMode ()
        }
    }
    
    func actionModeFunc(_ sender: Any){fatalError("Must Override")}
    @objc func refreshList(){fatalError("Must Override")}
    
    
    
    //UISearchBarDelegate functions
    func filterContentForSearchText(_ searchText: String, scope: Int = 0) {fatalError("Must Override")}
    
    func searchBarIsEmpty() -> Bool {
        // Returns true if the text is empty or nil
        return self.search.searchBar.text?.isEmpty ?? true
    }
    
    func displaySections() -> Bool {
        if IS_MULTIPLE_SECTIONS{
            let searchBarScopeIsDisplayingAll = self.search.searchBar.selectedScopeButtonIndex == 0
            return searchBarIsEmpty() && searchBarScopeIsDisplayingAll 
        }
        return false
    }
    
    func getItem(indexPath: IndexPath) -> Any{
        let itemsArray : [Any]?
        if  !displaySections(){
            itemsArray = filteredData
        } else{
            let key = headersArray[indexPath.section]
            itemsArray = sectionsMap[key]
        }
        return  itemsArray![indexPath.row]
    }
    
    func setupConstraints(){
        
        if DISPLAY_ANIMATED_MENU{
        parentView.addConstraints([
        NSLayoutConstraint(item: animatedMenu, attribute: .top, relatedBy: .equal, toItem: parentView.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: animatedMenu, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: animatedMenu, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: animatedMenu, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 40)
        ])
        }
        
        parentView.addConstraints([
        NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: parentView.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: DISPLAY_ANIMATED_MENU ? 40 : 0),
        NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: tableView, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: tableView, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0)
        ])
        
        parentView.addConstraints([
        NSLayoutConstraint(item: emptyListView, attribute: .top, relatedBy: .equal, toItem: parentView.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: DISPLAY_ANIMATED_MENU ? 40 : 0),
        NSLayoutConstraint(item: emptyListView, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: emptyListView, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: emptyListView, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0)
        ])
        
        parentView.addConstraints([
        NSLayoutConstraint(item: loaderView, attribute: .top, relatedBy: .equal, toItem: parentView.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant:0),
        NSLayoutConstraint(item: loaderView, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: loaderView, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0),
        NSLayoutConstraint(item: loaderView, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0)
        ])
    }
}

extension ParentListVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        if !displaySections(){
            return 1
        }
        return headersArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        if !displaySections(){
            return filteredData.count
        }
        
        let key = headersArray[section]
        if let data = sectionsMap[key]{
            return data.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return displaySections() ? 30 : 0
    }
    
    //the selection aside
    func sectionIndexTitles(for tableView: UITableView) -> [String]? {
        if !displaySections() || !DISPLAY_INDEX_TITLE{
            return nil
        }
        return self.headersArray
    }
    
    func tableView(_ tableView: UITableView,
                   titleForHeaderInSection section: Int) -> String?{
        if !displaySections(){
            return nil
        }
        return self.headersArray[section]
    }
}

extension ParentListVC: UISearchBarDelegate {
    // MARK: - UISearchBar Delegate
    func searchBar(_ searchBar: UISearchBar, selectedScopeButtonIndexDidChange selectedScope: Int) {
        filterContentForSearchText(searchBar.text!, scope: selectedScope)
    }
}

extension ParentListVC: UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        let searchBar = self.search.searchBar
        let scope = searchBar.selectedScopeButtonIndex
        filterContentForSearchText(searchBar.text!, scope: scope)
    }
}


