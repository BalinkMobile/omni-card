//
//  DragViewController.swift
//  Clientleling Template
//
//  Created by Shlomo Ariel on 22/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class DragViewController: UIViewController {
    var container: UIView!
    
    var panGesture = UIPanGestureRecognizer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.draggedView(_:)))
        container.addGestureRecognizer(panGesture)
    }
    
    @objc func draggedView(_ sender:UIPanGestureRecognizer){
        container.sliderFromBottomDrag(sender:sender, parentView:view)
    }
}
