//
//  FormUtils.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 23/08/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation
import UIKit

enum FormFieldType{
    case TextField
    case Picklist
    case Lookup
    case Date
    case Switch
    case TextEditor
    case Empty
    case List
}

class GenericFormUtil{
    static func itemsToCellConfigurator(items: [(label:String, value:String)]?) -> [CellConfigurator]{
        var cellItems = [CellConfigurator]()
        
        if let items = items{
            cellItems = items.map {
                return StandardListConfigurator(item: ($0))
            }
        }
        
        return cellItems
    }
}

class FieldFormSet {
    var title: String!
    var titleLbl: UILabel!
    var fieldName: String!
    var type: FormFieldType
    var isEnabled: Bool! = true
    var required: Bool! = false
    
    init(title:String, fieldName: String, type: FormFieldType, isEnabled:Bool = true, required: Bool = false){
        self.title = title
        self.fieldName = fieldName
        self.type = type
        self.isEnabled = isEnabled
        self.required = required
        
        self.titleLbl = UILabel()
        titleLbl.textColor = #colorLiteral(red: 0.6, green: 0.462745098, blue: 0.2941176471, alpha: 1)
        titleLbl.font = UIFont (name: "Helvetica", size: 11)
        self.titleLbl.text = self.title + (self.required ? " *" : "")
        self.titleLbl.setColor(word: "*", color: #colorLiteral(red: 0.9254902005, green: 0.2352941185, blue: 0.1019607857, alpha: 1))
    }
}

class TextFormField: FieldFormSet{
    var keyboardType: UIKeyboardType! = .default
    
    init(title: String, fieldName: String, type: FormFieldType = .TextField, isEnabled: Bool = true, required: Bool = false, keyboardType: UIKeyboardType = .default) {
        super.init(title: title, fieldName: fieldName, type: type, isEnabled: isEnabled, required: required)
        self.keyboardType = keyboardType
    }
}

class PicklistFormField: FieldFormSet{
    var picklistObjectName: String?
    var picklistFieldName: String?
    
    init(title: String, fieldName: String, type: FormFieldType = .Picklist, isEnabled: Bool = true, required: Bool = false, picklistObjectName: String? = nil, picklistFieldName: String? = nil) {
        super.init(title: title, fieldName: fieldName, type: type, isEnabled: isEnabled, required: required)
        self.picklistObjectName = picklistObjectName
        self.picklistFieldName = picklistFieldName
    }
}

class DateFormField: FieldFormSet{
    var datePickerMode: UIDatePickerMode! = .date
    
    init(title: String, fieldName: String, type: FormFieldType = .Date, isEnabled: Bool = true, required: Bool = false, datePickerMode: UIDatePickerMode = .date) {
        super.init(title: title, fieldName: fieldName, type: type, isEnabled: isEnabled, required: required)
        self.datePickerMode = datePickerMode
    }
}

class TextEditorFormField: FieldFormSet{
    var height: Int = 80
    
    init(title: String, fieldName: String, type: FormFieldType = .TextEditor, isEnabled: Bool = true, required: Bool = false, height: Int? = nil) {
        super.init(title: title, fieldName: fieldName, type: type, isEnabled: isEnabled, required: required)
        if let height = height {
            self.height = height
        }
    }
}

class ListFormField: FieldFormSet{
    var anyItems: [Any]?
    var items: [CellConfigurator]?
    var isMultiSelection : Bool?
    
    var listController = "genericListVC"
    
    init(title: String, fieldName: String, type: FormFieldType = .List, isEnabled: Bool = true, isMultiSelection: Bool = false, items: [CellConfigurator] = []) {
        self.items = items
        self.isMultiSelection = isMultiSelection
        super.init(title: title, fieldName: fieldName, type: type, isEnabled: isEnabled)
    }
    
    func getInitLabels(value: Any?) ->[String]?{
        if let value = value as? String{
             return [value]
        }
        
        return nil
    }
    
    func getInitValues(value: Any?) ->Any?{
        return value
    }
    
}
/*
class PicklistFormFields: ListFormField{
    var picklistObjectName: String?
    var picklistFieldName: String?
    
    init(title: String, fieldName: String, type: FormFieldType = .Picklist, isEnabled: Bool = true, isMultiSelection: Bool = false, picklistObjectName: String, picklistFieldName: String, items: [CellConfigurator] = []) {
        var cellItems = items
        if cellItems.count == 0 && isEnabled{
            cellItems = [CellConfigurator]()
            if let picklistItems = PickListObject.getValues(for: picklistFieldName, in: picklistObjectName, with: nil){
                for picklistItem in picklistItems {
                    cellItems.append(StandardListConfigurator(item: picklistItem))
                }
            }
        }
        
        self.picklistObjectName = picklistObjectName
        self.picklistFieldName = picklistFieldName
        super.init(title: title, fieldName: fieldName, type: .List, isEnabled: isEnabled, isMultiSelection: isMultiSelection, items: cellItems)
    }
    
    override func getInitLabels(value: Any?) ->[String]?{
        if let values = value as? [String]{
            return ConfigurationUtils.getPicklistLabels(objectName: picklistObjectName!, fieldName: picklistFieldName!, values: values)
        }
        return nil
    }
    
    override func getInitValues(value: Any?) ->Any?{
        if let value = value as? String{
            return value.components(separatedBy: ";")
        }
        if let values = value as? [String]{
            return values
        }
        return []
    }
}

class LabelValueListFormFields: ListFormField{
    var listItems: [(label:String, value:String)]?
    
    init(title: String, fieldName: String, type: FormFieldType = .List, isEnabled: Bool = true, isMultiSelection: Bool = false, items: [(label:String, value:String)] = []) {
        self.listItems = items
        var cellItems = [CellConfigurator]()
        
        if let listItems = listItems, isEnabled{
            cellItems = listItems.map {
                return StandardListConfigurator(item: ($0))
            }
        }
        super.init(title: title, fieldName: fieldName, type: .List, isEnabled: isEnabled, isMultiSelection: isMultiSelection, items: cellItems)
    }
    
    override func getInitLabels(value: Any?) ->[String]?{
        if let values = value as? [String], let listItems = listItems{
            var result = [String]()
            for listItem in listItems{
                if(values.contains(listItem.value)){
                    result.append(listItem.label)
                }
            }
            
            return result
        }
        return nil
    }
    
    /*override func getInitValues(value: Any?) ->[String]?{
        if let value = value as? String{
            return value.components(separatedBy: ";")
        }
        if let values = value as? [String]{
            return values
        }
        return []
    }*/
}

class FetchControllerFormFields: ListFormField{
    init(title: String, fieldName: String, isEnabled: Bool = true, isMultiSelection: Bool = false, listController: String) {
        super.init(title: title, fieldName: fieldName, isEnabled: isEnabled, isMultiSelection: isMultiSelection)
        self.listController = listController
    }
}
 */
