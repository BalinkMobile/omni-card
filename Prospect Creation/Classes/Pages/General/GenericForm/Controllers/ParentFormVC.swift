//
//  ParentFormVC.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 23/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//


import UIKit
import CoreData

class ParentFormVC: UIViewController {
    let LIST_LABELS_SUFFIX = "_LBL"
    var LOADING_PLACEHOLDER :String { get { return "".localized()}}
    
    var setFields = [String: [FieldFormSet]]()
    var sectionsArray = [String]()
    
    var openedDateCellIndexPath : IndexPath? = nil
    var selectedFieldSet : FieldFormSet? = nil
    var dataMap = [String: Any]()
    
     var parentView: UIView!
    var tableView: UITableView = UITableView()
    var loaderView: Loader!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        self.tableView.tableFooterView = UIView()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.bounces = false
        
        self.registerFormCells()
    }
    
    override func updateViewConstraints() {
        super.updateViewConstraints()
        
        parentView.addConstraints([
            NSLayoutConstraint(item: tableView, attribute: .top, relatedBy: .equal, toItem: parentView.safeAreaLayoutGuide, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .bottom, relatedBy: .equal, toItem: parentView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .left, relatedBy: .equal, toItem: parentView, attribute: .left, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: tableView, attribute: .right, relatedBy: .equal, toItem: parentView, attribute: .right, multiplier: 1, constant: 0)
            ])
    }
    
    func initForm(_ containerView: UIView? = nil){
        /*let bounds: CGRect = containerView != nil ? containerView!.bounds: self.view.bounds
        let boundsWidth = bounds.size.width
        let boundsHeight = bounds.size.height
        self.tableView.frame = CGRect(x:0, y:0, width:boundsWidth + 30, height:boundsHeight)
        
        if let _ = containerView{
            containerView?.addSubview(self.tableView)
        } else{
            self.view.addSubview(self.tableView)
        }*/
        if let _ = containerView{
            parentView = containerView!
            
        } else{
            parentView = self.view
        }
        
        self.setPageLoader()
        parentView.addSubview(self.tableView)
        tableView.translatesAutoresizingMaskIntoConstraints = false
    }
    
    func registerFormCells(){
        let cells = ["TextFieldFormCell", "SwitchFormCell","DateFormCell","PicklistFormCell", "TextEditorFormCell", "EmptyFormCell", "ListFormCell"]
        for cell in cells{
            let cellNib = UINib(nibName: cell, bundle: nil)
            tableView.register(cellNib, forCellReuseIdentifier: cell)
        }
    }
    
    func pushPicklistController(){
        let storyboard = UIStoryboard(name: "General", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "picklistSelection") as! PickListSelectionVC
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    func isValidForm() -> Bool {
        for fieldsInSection in setFields.values {
            for field in fieldsInSection {
                if field.required {
                    if let value = self.dataMap[field.fieldName] {
                        if let valueStr = value as? String, valueStr == "" {
                            return false
                        }
                    } else {
                        return false
                    }
                }
            }
        }
        return true
    }
    
    func onFieldChange(fieldSet: FieldFormSet, value: AnyObject?, label: AnyObject? = nil) {
        self.dataMap[fieldSet.fieldName] = value
        if fieldSet.type == .Date {
            self.tableView.reloadData()
        }
        
        if let label = label {
            self.dataMap[fieldSet.fieldName + LIST_LABELS_SUFFIX] = label
            self.tableView.reloadData()
        }
    }
    
    func setPageLoader() {
        loaderView = Loader(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        loaderView?.textDisplayed = LOADING_PLACEHOLDER
        loaderView?.alphaValue = 0.8
        loaderView?.hide()
        self.view.addSubview(loaderView!)
    }
    
    func showLoader(_ show: Bool) {
        loaderView?.isLoading = show
    }
    
}

extension ParentFormVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int{
        return self.sectionsArray.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int{
        let currentSection = self.sectionsArray[section]
        
        return setFields[currentSection]?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell{
        var cell = ParentFormCell()
        let currentSection = self.sectionsArray[indexPath.section]
        if let fieldSet = setFields[currentSection]?[indexPath.row]{
            switch fieldSet.type{
            case .Date:
                cell = tableView.dequeueReusableCell(withIdentifier: "DateFormCell") as! DateFormCell
            case .Switch:
                cell = tableView.dequeueReusableCell(withIdentifier: "SwitchFormCell") as! SwitchFormCell
            case .TextField:
                cell = tableView.dequeueReusableCell(withIdentifier: "TextFieldFormCell") as! TextFieldFormCell
            case .TextEditor:
                cell = tableView.dequeueReusableCell(withIdentifier: "TextEditorFormCell") as! TextEditorFormCell
            case .Empty:
                cell = tableView.dequeueReusableCell(withIdentifier: "EmptyFormCell") as! EmptyFormCell
            case .List:
                cell = tableView.dequeueReusableCell(withIdentifier: "ListFormCell") as! ListFormCell
            default:
                break
            }
            
            cell.fieldSet = fieldSet
            if fieldSet.type == .Lookup || fieldSet.type == .List{
                if let listFieldSet = fieldSet as? ListFormField{
                    let values = listFieldSet.getInitValues(value: self.dataMap[fieldSet.fieldName])
                    let labels = self.dataMap[fieldSet.fieldName + LIST_LABELS_SUFFIX] ?? listFieldSet.getInitLabels(value: values) ?? values as Any 
                    cell.value = (labels: labels, values: values) as AnyObject
                }
            } else{
                cell.value = self.dataMap[fieldSet.fieldName]
            }
            
            cell.saveData = {(key,value) in
                self.onFieldChange(fieldSet: fieldSet, value: value)
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath){
        
        let currentSection = self.sectionsArray[indexPath.section]
        if let fieldSet = setFields[currentSection]?[indexPath.row]{
            switch fieldSet.type{
            case .List:
                let cell:ListFormCell = tableView.cellForRow(at: indexPath) as! ListFormCell
                var cellData: (labels: [String]?, values: [String]?)
                if let data = cell.value as? (labels: [String]?, values: [String]?){
                    cellData = data
                } else if let data = cell.value as? (labels: String, values: String){
                    cellData = (labels: [data.labels], values: [data.values])
                }
                self.selectedFieldSet = fieldSet
                if let listFieldSet = fieldSet as? ListFormField{
                    pushRelatedToController(fieldSet: listFieldSet, selectedValues: cellData.values, selectedLabels: cellData.labels)
                }
            case .Date:
                let openIndexPath = openedDateCellIndexPath == indexPath ? nil : indexPath
                openedDateCellIndexPath = openIndexPath
            default:
            break
            }
            
            if fieldSet.type != .Date{
                openedDateCellIndexPath = nil
            }
        }
        
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    func toggleDateCell(_ indexPath: IndexPath?){
        self.openedDateCellIndexPath = indexPath
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat{
        return self.sectionsArray[section] != "" ? 30 : 0
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let returnedView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 30))
        returnedView.backgroundColor = #colorLiteral(red: 0.937254902, green: 0.937254902, blue: 0.9568627451, alpha: 1)
        
        let label = UILabel(frame: CGRect(x: 20, y: 0, width: view.frame.size.width, height: 30))
        label.text = self.sectionsArray[section].uppercased()
        label.font = UIFont.systemFont(ofSize: 11, weight: .semibold)
        label.textColor = #colorLiteral(red: 0.4196078431, green: 0.4196078431, blue: 0.4196078431, alpha: 1)
        returnedView.addSubview(label)

        return returnedView
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        let currentSection = self.sectionsArray[indexPath.section]
        if let fieldSetType = setFields[currentSection]?[indexPath.row].type {
            if fieldSetType == .Date, openedDateCellIndexPath == indexPath {
                return 210
            }
            if fieldSetType == .Empty {
                return 25
            }
            if fieldSetType == .TextEditor {
                let fieldSet = setFields[currentSection]?[indexPath.row]
                return CGFloat((fieldSet as! TextEditorFormField).height)
            }
        }
        return 57
        
    }
    
    func pushRelatedToController(fieldSet: ListFormField, selectedValues: [String]?, selectedLabels: [String]?){
        let storyboard = UIStoryboard(name: "GenericForm", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: fieldSet.listController) as! GenericListVC
        
        controller.items = fieldSet.items ?? []
        controller.IS_MULTISELECTION = fieldSet.isMultiSelection ?? false
        controller.selectedValues = selectedValues ?? []
        controller.selectedLabels = selectedLabels ?? []
        
        controller.itemListSelected = self
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension ParentFormVC: ItemListSelectionDelegate{
    func dataSelected(labels: [String]?, values: [String]?) {
        if let selectedFieldSet = self.selectedFieldSet, let labels = labels, let values = values{
            self.onFieldChange(fieldSet: selectedFieldSet, value: values as AnyObject, label: labels as AnyObject)
            self.selectedFieldSet = nil
        }
    }
}



