//
//  GenericListVC.swift
//  Chaumet DEV
//
//  Created by elie buff on 21/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

protocol ItemListSelectionDelegate {
    func dataSelected(labels: [String]?, values: [String]?)
}

class GenericListVC: ParentListVC {
    var IS_MULTISELECTION = false
    
    var items = [CellConfigurator]()
    var itemListSelected: ItemListSelectionDelegate?
    var selectedLabels = [String]()
    var selectedValues = [String]()
    
    override var NO_DATA_TITLE: String { get { return "No items".localized()}}
    override var NO_DATA_SUBTITLE : String { get { return "No items available for selection".localized()}}
    override var SEARCH_PLACEHOLDER: String { get {return "Search".localized()}}
    override var LOADING_PLACEHOLDER: String { get {return "Loading Items".localized()}}
    override var IS_REFRESHABLE_LIST:Bool { get { return false}}
    override var ADD_SEARCHBAR :Bool { get { return false}}
    override var IS_MULTIPLE_SECTIONS:Bool { get { return false}}
    override var IS_LARGE_TITLE: Bool { get { return false}}
    var ADD_RIGHT_BUTTON :Bool { get { return false}}
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .done, target: self, action: #selector(self.dismissView))
        self.navigationItem.setLeftBarButton(leftBarButtonItem, animated: true)
        if IS_MULTISELECTION || ADD_RIGHT_BUTTON{
            let rightBarButtonItem = UIBarButtonItem(title: "Done", style: .done, target: self, action: #selector(self.doneSelection))
            self.navigationItem.setRightBarButton(rightBarButtonItem, animated: true)
        }
        tableView.allowsMultipleSelection = IS_MULTISELECTION
        self.allData = items as [AnyObject]
        
        let cellNib = UINib(nibName: "StandardListCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "StandardListCell")
        
        initTable()
    }
    
    @objc func doneSelection(){
        itemListSelected?.dataSelected(labels: selectedLabels, values: selectedValues)
        dismissView()
    }
    
    @objc func dismissView(){
        _ = self.navigationController?.popViewController(animated: true)
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = items[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId)!
        item.configure(cell: cell)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ParentGenericListCell else {
            return
        }
        let selectedItem = cell.getSelectedData()
        
        if !IS_MULTISELECTION{
            itemListSelected?.dataSelected(labels: [selectedItem.label], values: [selectedItem.value])
            dismissView()
        }else{
            selectedLabels.append(selectedItem.label)
            selectedValues.append(selectedItem.value)
        }
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) as? ParentGenericListCell else {
            return
        }
        let selectedItem = cell.getSelectedData()
        
        if let index = selectedValues.index(where: {$0 == selectedItem.value}){
            selectedValues.remove(at: index)
            selectedLabels.remove(at: index)
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard let cell = cell as? ParentGenericListCell else {
            return
        }
        let currentCellValue = cell.getSelectedData().value
        
        if selectedValues.contains(currentCellValue) {
            tableView.selectIndex(indexPath: indexPath)
        }
    }
}
