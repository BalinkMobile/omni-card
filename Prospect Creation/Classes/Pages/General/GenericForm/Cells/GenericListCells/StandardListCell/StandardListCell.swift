//
//  Standard.swift
//  Chaumet DEV
//
//  Created by elie buff on 21/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

typealias StandardListConfigurator =  TableCellConfigurator<StandardListCell, (label:String, value: String)>

class StandardListCell: ParentGenericListCell, ConfigurableCell {
    
    @IBOutlet weak var titleLbl: UILabel!
    var value: String!
    
    func configure(data cellData: (label:String, value: String)) {
        titleLbl.text = cellData.label
        value = cellData.value
    }
    
    override func getSelectedData() ->(label: String, value: String){
        return (label: titleLbl.text ?? "", value: value)
        
    }
    
}
