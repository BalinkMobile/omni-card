//
//  ParentGenericListCell.swift
//  Chaumet DEV
//
//  Created by elie buff on 22/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class ParentGenericListCell: UITableViewCell {
    func getSelectedData() ->(label: String, value: String){
        fatalError("Must Override")
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        accessoryType = selected ? .checkmark : .none
    }
}
