//
//  TextFieldFormCell.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 23/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class TextFieldFormCell: ParentFormCell {
    
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var valueLbl: UITextField!

    @IBAction func editingChanged(_ sender: UITextField) {
        self.saveData!(self.fieldSet.fieldName, sender.text as AnyObject)
    }
    
    override func setFieldSet() {
        self.titlelbl.attributedText = self.fieldSet.titleLbl.attributedText
        self.valueLbl.isEnabled = self.fieldSet.isEnabled
        self.valueLbl.keyboardType = (self.fieldSet as! TextFormField).keyboardType
    }
    
    override func setValue() {
        self.valueLbl.text = "\(self.value ?? "")"
    }
    
}
