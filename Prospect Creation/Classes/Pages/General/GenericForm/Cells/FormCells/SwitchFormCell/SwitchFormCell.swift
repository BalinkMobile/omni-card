//
//  SwitchFormCell.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 23/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class SwitchFormCell: ParentFormCell {

    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var switchLbl: UISwitch!
    
    @IBAction func switchDidChanged(_ sender: UISwitch) {
        self.saveData!(self.fieldSet.fieldName, sender.isOn as AnyObject)
    }
    
    override func setFieldSet() {
        self.titlelbl.attributedText = self.fieldSet.titleLbl.attributedText
        self.switchLbl.isEnabled = self.fieldSet.isEnabled
    }
    
    override func setValue() {
        self.switchLbl.isOn = self.value as? Bool ?? false
    }
    
}
