//
//  DateFormCell.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 23/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class DateFormCell: ParentFormCell {
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    @IBOutlet weak var datePickerHeight: NSLayoutConstraint!
    @IBOutlet weak var datePicker: UIDatePicker!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func dateDidChanged(_ sender: UIDatePicker) {
        self.saveData!(self.fieldSet.fieldName, sender.date as AnyObject)
        
    }
    
    
    override func setFieldSet() {
        self.titlelbl.attributedText = self.fieldSet.titleLbl.attributedText
        self.datePicker.datePickerMode = (self.fieldSet as! DateFormField).datePickerMode
        self.isUserInteractionEnabled = self.fieldSet.isEnabled
    }
    
    override func setValue() {
        let dateFormat: DateFormat = (self.fieldSet as! DateFormField).datePickerMode == .date ? .shortDate : .dateTime
        self.valueLbl.text = (self.value as? Date)?.toString(format: dateFormat)
        self.datePicker.date = self.value as? Date ?? Date()
    }
    
}
