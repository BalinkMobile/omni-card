//
//  ListFormCell.swift
//  Chaumet DEV
//
//  Created by elie buff on 21/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class ListFormCell: ParentFormCell {
    @IBOutlet weak var titlelbl: UILabel!
    @IBOutlet weak var valueLbl: UILabel!
    
    override func setFieldSet() {
        self.titlelbl.attributedText = self.fieldSet.titleLbl.attributedText
        self.accessoryType = self.fieldSet.isEnabled ? .disclosureIndicator : .none
        self.isUserInteractionEnabled = self.fieldSet.isEnabled
    }
    
    override func setValue() {
        self.valueLbl.text = ""
        if let data = self.value as? (labels: [String], values: [String]){
            if data.values.count == 1{
                self.valueLbl.text = data.labels[0]
            }else if data.values.count > 1{
                self.valueLbl.text = "\(data.values.count) items"
            }
        } else  if let data = self.value as? (labels: String, values: String){
            self.valueLbl.text = data.labels
        }
    }
}
