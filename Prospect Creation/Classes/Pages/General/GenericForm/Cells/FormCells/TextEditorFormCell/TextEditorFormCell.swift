//
//  TextEditorFormCell.swift
//  Clientleling Template
//
//  Created by Nehora Sharabi on 16/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class TextEditorFormCell: ParentFormCell, UITextViewDelegate {
    
    @IBOutlet weak var textView: UITextView!
    
    func textViewDidChange(_ textView: UITextView) {
        self.saveData!(self.fieldSet.fieldName, textView.text as AnyObject)
    }

    override func setFieldSet() {
        self.textView.toolbarPlaceholder = self.fieldSet.title
        self.textView.isEditable = self.fieldSet.isEnabled
    }

    override func setValue() {
        self.textView.text = self.value as? String
    }
    
}
