//
//  ParentFormCell.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 23/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class ParentFormCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    var saveData: ((String, AnyObject?) -> ())?
    
    var fieldSet: FieldFormSet!{
        didSet{
            setFieldSet()
        }
    }
    
    var value: Any?{
        didSet{
            setValue()
        }
    }
    
    
    func setFieldSet(){fatalError("Must Override")}
    func setValue(){fatalError("Must Override")}

}
