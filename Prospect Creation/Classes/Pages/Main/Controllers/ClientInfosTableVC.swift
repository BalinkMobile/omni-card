//
//  ClientInfosTableVC.swift
//  Prospect Creation
//
//  Created by Jeremy Martiano on 10/12/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import ReSwift


class ClientInfosTableVC: UITableViewController {
    let PROSPECT_REQUIRED_FIELDS = ["lastName"]
    let CHECKBOX_VALUES = ["optInNewsletter", "optInPhone1", "optInEmail1", "optInInstantMessage", "optInMailing"]
    var popPicker : PopPickerManager?
    var fields = [String:(label: String, value: AnyObject)]()
    var collapsedRows = [Int]()
    
    @IBOutlet weak var salutationLbl: UITextField!
    @IBOutlet weak var firstNameLbl: UITextField!
    @IBOutlet weak var lastNameLbl: UITextField!
    @IBOutlet weak var adressLbl: UITextField!
    @IBOutlet weak var zibLbl: UITextField!
    @IBOutlet weak var cityLbl: UITextField!
    @IBOutlet weak var countryLbl: UITextField!
    @IBOutlet weak var nationalityLbl: UITextField!
    @IBOutlet weak var countryCodePortableLbl: UITextField!
    @IBOutlet weak var portableLbl: UITextField!
    @IBOutlet weak var countryCodeFixLbl: UITextField!
    @IBOutlet weak var fixLbl: UITextField!
    @IBOutlet weak var emailLbl: UITextField!
    @IBOutlet weak var instantMessagingListLbl: UITextField!
    @IBOutlet weak var instantMessagingLbl: UITextField!
    @IBOutlet weak var birthdayLbl: UITextField!
    @IBOutlet weak var weddingAnniversaryLbl: UITextField!
    @IBOutlet weak var createProspectButton: RoundedButton!
    @IBOutlet weak var adress2Lbl: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupHideKeyboardOnTap()
        salutationLbl.delegate = self
        salutationLbl.withImageName(imageName: "shape", direction: .Right, imgSize: (width: 10, height: 6))
        firstNameLbl.delegate = self
        lastNameLbl.delegate = self
        adressLbl.delegate = self
        adress2Lbl.delegate = self
        zibLbl.delegate = self
        cityLbl.delegate = self
        countryLbl.delegate = self
        countryLbl.withImageName(imageName: "shape", direction: .Right, imgSize: (width: 10, height: 6))
        nationalityLbl.delegate = self
        nationalityLbl.withImageName(imageName: "shape", direction: .Right, imgSize: (width: 10, height: 6))
        portableLbl.delegate = self
        fixLbl.delegate = self
        emailLbl.delegate = self
        instantMessagingListLbl.delegate = self
        instantMessagingListLbl.withImageName(imageName: "shape", direction: .Right, imgSize: (width: 10, height: 6))
        instantMessagingLbl.delegate = self
        birthdayLbl.delegate = self
        weddingAnniversaryLbl.delegate = self
        countryCodePortableLbl.delegate = self
        countryCodePortableLbl.withImageName(imageName: "shape", direction: .Right, imgSize: (width: 10, height: 6))
        countryCodeFixLbl.delegate = self
        countryCodeFixLbl.withImageName(imageName: "shape", direction: .Right, imgSize: (width: 10, height: 6))
        
    }
    @IBAction func contactChannelSwitch(_ sender: UISwitch) {
            let selectedField = CHECKBOX_VALUES[sender.tag]
            ProspectUtils.updateProspectField(fields: [selectedField: (label: "", value: sender.isOn as AnyObject)])
    }
    
    @IBAction func createProspectClicked(_ sender: Any) {
        self.parent?.performSegue(withIdentifier: "showSignature", sender: self)
    }
    
    @IBAction func toggleRow(_ sender: UIButton) {
        if let index = collapsedRows.firstIndex(of: sender.tag){
            collapsedRows.remove(at: index)
        } else{
            collapsedRows.append(sender.tag)
        }
        tableView.beginUpdates()
        tableView.endUpdates()
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if collapsedRows.contains(indexPath.row){
            return 140
        }
        return 65
    }
    
    
}

extension ClientInfosTableVC: StoreSubscriber{
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        mainStore.subscribe(self){ subcription in
            subcription.select { state in state.prospectState }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    func newState(state: ProspectState){
        if let fields = state.prospectFields{
            
            self.fields = fields
            setDefaultCountryCode()
            loadExistingFields(fields: self.fields)
            
        }
        
        checkFormValidity(fields: state.prospectFields, signature: state.imgSignature)
    }
    
    func setDefaultCountryCode(){
        if self.fields["phone1CountryCode"] == nil && self.fields["mobile1CountryCode"] == nil{
            if let countryCodeUser = UserDefaultUtils.getItem(key: .storeCountry) as? String, let label = ConfigurationUtils.getSelectedPicklistValuesLabelsArray(objectName: "Account", fieldName: "Mobile1CountryCode__pc", selectedValues: countryCodeUser)?.first?.label{
                ProspectUtils.updateProspectField(fields: ["phone1CountryCode": ( label: label, value: countryCodeUser as AnyObject ),"mobile1CountryCode": ( label: label, value: countryCodeUser as AnyObject )])
            }
            
        }
    }
    
    func loadExistingFields(fields: [String:(label: String, value: AnyObject)]){
        
        firstNameLbl.text = fields["firstName"]?.label
        lastNameLbl.text = fields["lastName"]?.label
        adressLbl.text = fields["billingLine1"]?.label
        adress2Lbl.text = fields["billingLine2"]?.label
        zibLbl.text = fields["zipCode"]?.label
        cityLbl.text = fields["city"]?.label
        portableLbl.text = fields["mobile"]?.label
        fixLbl.text = fields["phone"]?.label
        emailLbl.text = fields["email"]?.label
        instantMessagingLbl.text = fields["instantMessageId"]?.label
        birthdayLbl.text = fields["birthdateDay"]?.label
        weddingAnniversaryLbl.text = fields["weddingDay"]?.label
        salutationLbl.text = fields["salutation"]?.label
        countryLbl.text = fields["country"]?.label
        nationalityLbl.text = fields["nationality"]?.label
        instantMessagingListLbl.text = fields["instantMessage"]?.label
        countryCodePortableLbl.text = fields["mobile1CountryCode"]?.label
        countryCodeFixLbl.text = fields["phone1CountryCode"]?.label
        
    }
    
    func checkFormValidity(fields: [String:(label: String, value: AnyObject)]?, signature :UIImage?){
        
        createProspectButton.isEnabled = false
        createProspectButton.backgroundColor = #colorLiteral(red: 0.8431372549, green: 0.8431372549, blue: 0.8431372549, alpha: 1)
        
        guard let fields  = fields else{
            return
        }
        
        for field in PROSPECT_REQUIRED_FIELDS{
            guard let value = fields[field]?.label else{
                return
            }
            if value.isEmpty{
                return
            }
        }
        
        createProspectButton.isEnabled = true
        createProspectButton.backgroundColor = #colorLiteral(red: 0.6431372549, green: 0.5058823529, blue: 0.337254902, alpha: 1)
        
    }
}

extension ClientInfosTableVC : UITextFieldDelegate{
    
    func textFieldDidEndEditing(_ textField: UITextField, reason: UITextFieldDidEndEditingReason) {
        
        var fieldKey = ""
        
        if textField == firstNameLbl{
            fieldKey = "firstName"
        }
        else if textField == lastNameLbl{
            fieldKey = "lastName"
        }
        else if textField == adressLbl{
            fieldKey = "billingLine1"
        }
        else if textField == adress2Lbl{
            fieldKey = "billingLine2"
        }
        else if textField == zibLbl{
            fieldKey = "zipCode"
        }
        else if textField == cityLbl{
            fieldKey = "city"
        }
        else if textField == portableLbl{
            fieldKey = "mobile"
        }
        else if textField == fixLbl{
            fieldKey = "phone"
        }
        else if textField == emailLbl{
            fieldKey = "email"
        }
        else if textField == instantMessagingLbl{
            fieldKey = "instantMessageId"
        } else if textField == countryCodeFixLbl{
            fieldKey = "phone1CountryCode"
            
        } else if textField == countryCodePortableLbl{
            fieldKey = "mobile1CountryCode"
        }
        
        let text = textField.text ?? ""
        
        ProspectUtils.updateProspectField(fields: [fieldKey : (label: text, value: text as AnyObject)])
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if (textField ===  birthdayLbl) {
            initCustomDatePopoverDisplay(textField, fieldsName: ["birthdateDay", "birthdateMonth", "birthdateYear"], popoverDir : .down)
            return false
        }
        
        if (textField ===  weddingAnniversaryLbl) {
            initCustomDatePopoverDisplay(textField, fieldsName: ["weddingDay", "weddingMonth", "weddingYear"], popoverDir : .down, maxYear: 2022)
            return false
        }
        
        if (textField ===  salutationLbl) {
            let items = ConfigurationUtils.getSelectedPicklistValuesLabelsArray(objectName: "Account", fieldName: "Salutation", selectedValues: nil, onlyFromValues: true)
            initPicklistPopoverDisplay(textField, fieldName: "salutation", values: items, popoverDir: .down)
            return false
        }
        
        if (textField ===  countryLbl) {
            let items = ConfigurationUtils.getSelectedPicklistValuesLabelsArray(objectName: "Account", fieldName: "BillingCountry__pc", selectedValues: nil)
            initPicklistPopoverDisplay(textField, fieldName: "country", values: items, popoverDir: .down, setSearchBar: true)
            return false
        }
        
        if (textField ===  nationalityLbl) {
            let items = ConfigurationUtils.getSelectedPicklistValuesLabelsArray(objectName: "Account", fieldName: "Nationality__pc", selectedValues: nil)
            initPicklistPopoverDisplay(textField, fieldName: "nationality", values: items, popoverDir: .down, setSearchBar: true)
            return false
        }
        
        if (textField ===  instantMessagingListLbl) {
            let items = ConfigurationUtils.getSelectedPicklistValuesLabelsArray(objectName: "Account", fieldName: "InstantMessage__pc", selectedValues: nil)
            initPicklistPopoverDisplay(textField, fieldName: "instantMessage", values: items, popoverDir: .down)
            return false
        }
        
        if (textField ===  countryCodeFixLbl) {
            let items = ConfigurationUtils.getSelectedPicklistValuesLabelsArray(objectName: "Account", fieldName: "Mobile1CountryCode__pc", selectedValues: nil)
            initPicklistPopoverDisplay(textField, fieldName: "phone1CountryCode", values: items, popoverDir: .down, setSearchBar: true)
            return false
        }
        
        if (textField ===  countryCodePortableLbl) {
            let items = ConfigurationUtils.getSelectedPicklistValuesLabelsArray(objectName: "Account", fieldName: "Mobile1CountryCode__pc", selectedValues: nil)
            initPicklistPopoverDisplay(textField, fieldName: "mobile1CountryCode", values: items, popoverDir: .down, setSearchBar: true)
            return false
        }
        
        return true
    }
    
    func initCustomDatePopoverDisplay(_ txtField : UITextField, fieldsName: [String], popoverDir : UIPopoverArrowDirection, maxYear: Int? = nil){
        view.endEditing(true)
        popPicker = PopPickerManager(srcTextField: txtField)
        let selectDate = (day: self.fields[fieldsName[0]]?.value as? Int, month: self.fields[fieldsName[1]]?.value as? Int, year: self.fields[fieldsName[2]]?.value as? Int)
        popPicker?.customPickDate(self, width: CGFloat(350), popOverDir: popoverDir, selectedDate: selectDate,maxYear: maxYear, dataChanged:{ (label, value) -> () in
            txtField.text = label
            
            if let value = value as? [Int?]{
                var fieldsValues = [String: (label: String, value: AnyObject)]()
                for (index, fieldName) in fieldsName.enumerated(){
                    fieldsValues[fieldName] = (label: label, value: value[index] as AnyObject)
                }
                ProspectUtils.updateProspectField(fields: fieldsValues)
            }
        })
    }
    
    func initPicklistPopoverDisplay(_ txtField : UITextField, fieldName: String, values:[(label:String, value:String)]?, popoverDir : UIPopoverArrowDirection, setSearchBar: Bool = false){
        view.endEditing(true)
        popPicker = PopPickerManager(srcTextField: txtField)
        
        popPicker?.pickList(self, width: CGFloat(300), popOverDir: popoverDir, values: values, selectedValue: nil, setSearchBar: setSearchBar
            , dataChanged:{ (label, value) -> () in
                txtField.text = label
                ProspectUtils.updateProspectField(fields: [fieldName: (label: label, value: value)])
        })
    }
    
    
    
}
