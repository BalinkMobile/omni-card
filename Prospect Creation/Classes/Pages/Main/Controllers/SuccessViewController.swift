//
//  SuccessViewController.swift
//  Prospect Creation
//
//  Created by elie buff on 13/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class SuccessViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    @IBAction func backToRoot(_ sender: Any) {
        ProspectUtils.updateProspectField(fields: nil)
        ProspectUtils.addProspectSignature(signatureImage: nil)
        self.navigationController?.popToRootViewController(animated: false)
    }
}
