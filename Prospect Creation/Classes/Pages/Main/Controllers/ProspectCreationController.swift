//
//  ProspectCreationController.swift
//  Clientleling Template
//
//  Created by elie buff on 07/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import ReSwift
import PromiseKit
import SalesforceSDKCore


class ProspectCreationController: UIViewController {
    var selectedShop: Shop?
    var selectedUser: User?
    
    var loaderView: AdvancedLoader?
    

    @IBOutlet weak var selectedStoreLbl: UILabel!
    @IBOutlet weak var selectedSALbl: UILabel!
    @IBOutlet weak var userProfilePicture: RoundedImage!
    @IBOutlet weak var profilePictureLoader: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onLangButton(_ sender: UIButton) {
        ConfigurationUtils.changeLanguage(self, sender: sender)
    }

    
    @IBAction func onBackTap(_ sender: Any) {
        _ = self.navigationController?.popViewController(animated: true)
    }

}


extension ProspectCreationController: StoreSubscriber{
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        mainStore.subscribe(self)
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    func newState(state: State){
        
        if let storeName = state.userState.userInfo?[.storeName]{
            selectedStoreLbl.text = "\(storeName)"
        }
        
        if let selectedUser = state.configState.selectedUser{
            selectedSALbl.text = "\(selectedUser.firstName ?? "") \(selectedUser.lastName ?? "")"
            setUserPhoto(selectedUser)
        }
    }
    
    func setUserPhoto(_ currentUser: User){
        profilePictureLoader.startAnimating()
        self.profilePictureLoader.isHidden = false
        let accessToken = SFUserAccountManager.sharedInstance().currentUser?.credentials.accessToken
        let url = "\(currentUser.photoUrl ?? "")?oauth_token=\(accessToken ?? "")"
        
        userProfilePicture.kf.setImage(with: URL(string: url)!, placeholder: UIImage(named: "ProfileEx"), options: nil, progressBlock: nil, completionHandler: { (img, err , cache, nil) in
            if let _ = img{
                self.profilePictureLoader.stopAnimating()
                self.profilePictureLoader.isHidden = true
            }
        })
    }

}
