//
//  MainViewController.swift
//  IOS Template
//
//  Created by Shlomo Ariel on 31/05/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import ReSwift
import SalesforceSDKCore


class HomeViewController: UIViewController {
    let USER_CELL_WIDTH = 111
    
    var selectedShop: Shop?
    var users: [User]?
    var selectedUser: User?
    
    @IBOutlet weak var usersCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onLangButton(_ sender: UIButton) {
        ConfigurationUtils.changeLanguage(self, sender: sender)
    }
    
    @IBAction func onLogoutClick(_ sender: Any) {
        SFAuthenticationManager.shared().logout()
    }
}

extension HomeViewController: StoreSubscriber{
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        ConfigurationUtils.getLocalUsers()
        mainStore.subscribe(self)
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    func newState(state: State){
        
        if let availableUsers = state.configState.availableUsers, self.users == nil{
            self.users = availableUsers
            self.selectedUser = state.configState.selectedUser ?? users?.first
            usersCollectionView.reloadData()
        }

    }
}

extension HomeViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let users = users else{
            return 0
        }
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "userCell", for: indexPath) as! UserCell
        cell.currentUser = users?[indexPath.row]
        if self.selectedUser == users?[indexPath.row]{
            cell.mainViewWidth.constant = CGFloat(USER_CELL_WIDTH)
            cell.mainView.alpha = 1
        } else {
            cell.mainViewWidth.constant = CGFloat(USER_CELL_WIDTH) * 0.82
            cell.mainView.alpha = 0.5
        }
        cell.userProfilePicture.CornerRadius = Int(cell.userProfilePicture.frame.width / 2)
        cell.setNeedsDisplay()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.selectedUser = users![indexPath.row]
        
        ConfigurationUtils.selectUser(user: selectedUser!)
        usersCollectionView.reloadData()
    }
}

extension HomeViewController : UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let totalCellWidth = USER_CELL_WIDTH * collectionView.numberOfItems(inSection: 0)
        let totalSpacingWidth = 10 * (collectionView.numberOfItems(inSection: 0) - 1)
        
        let leftInset = (collectionView.layer.frame.size.width - CGFloat(totalCellWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        
        if Int(collectionView.layer.frame.size.width) < totalCellWidth{
            return UIEdgeInsetsMake(0, 0, 0, 0)
        }
        
        return UIEdgeInsetsMake(0, leftInset, 0, rightInset)
    }
}
