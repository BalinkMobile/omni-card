//
//  ContactInfoVC.swift
//  Prospect Creation
//
//  Created by elie buff on 10/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import Foundation
import PromiseKit
import ReSwift

class SignatureVC: UIViewController {
    var prospectFields : [String:(label: String, value: AnyObject)]?
    @IBOutlet weak var signatureView: YPDrawSignatureView!
    
    @IBOutlet weak var saveButton: RoundedButton!
    var loaderView: AdvancedLoader?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        saveButton.isEnabled = false
        signatureView.delegate = self
    }
    
    override func viewDidLayoutSubviews() {
        setPageLoader()
    }
    
    func setPageLoader() {
        loaderView = AdvancedLoader(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: self.view.frame.size.height))
        loaderView?.title = ""
        loaderView?.alpha = 0
        self.view.addSubview(loaderView!)
    }
    
    @IBAction func closeWindowCliked(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func createProspectClicked(_ sender: Any) {

        ProspectUtils.updateClientDetails()
            .then{_ -> Promise<Void> in
                if let nav = self.presentingViewController as? UINavigationController, let vc = nav.viewControllers.last as? ProspectCreationController{
                    self.dismiss(animated: true, completion: nil)
                    vc.performSegue(withIdentifier: "successSegue", sender: self)
                    
                }
                return Promise()
        }
        
    }
}

extension SignatureVC: StoreSubscriber{
    override func viewWillAppear(_ animated: Bool){
        super.viewWillAppear(animated)
        mainStore.subscribe(self){ subcription in
            subcription.select { state in state.prospectState }
        }
    }
    
    override func viewWillDisappear(_ animated: Bool){
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    func newState(state: ProspectState){
        if let isLoading = state.isLoading{
            loaderView?.isLoading = isLoading
        }
    }
}
extension SignatureVC: YPSignatureDelegate{
    func didStart(_ view: YPDrawSignatureView) {
        
    }
    
    func didFinish(_ view: YPDrawSignatureView) {
        saveButton.isEnabled = true
        saveButton.backgroundColor = #colorLiteral(red: 0.737254902, green: 0.5921568627, blue: 0.431372549, alpha: 1)
        ProspectUtils.addProspectSignature(signatureImage: signatureView.getSignature())
    }
}



