//
//  SelectSimpleTableCell.swift
//  Prospect Creation
//
//  Created by Jeremy Martiano on 16/12/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class SelectSimpleTableCell: UITableViewCell {
    @IBOutlet weak var checkImg: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
