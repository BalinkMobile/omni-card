//
//  UserCell.swift
//  Clientleling Template
//
//  Created by elie buff on 06/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit
import SalesforceSDKCore

class UserCell: UICollectionViewCell {
    @IBOutlet weak var mainViewWidth: NSLayoutConstraint!
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var usernameLbl: UILabel!
    @IBOutlet weak var profileContainer: RoundedView!
    @IBOutlet weak var userProfilePicture: RoundedImage!
    @IBOutlet weak var profilePictureLoader: UIActivityIndicatorView!
   
    override func draw(_ rect: CGRect) {
        userProfilePicture.CornerRadius = Int(userProfilePicture.frame.width / 2)
    }
    
    var currentUser: User!{
        didSet{
            self.usernameLbl.text = "\(currentUser.firstName ?? "")   \(currentUser.lastName ?? "")"
            
            
            profilePictureLoader.startAnimating()
            self.profilePictureLoader.isHidden = false
            let accessToken = SFUserAccountManager.sharedInstance().currentUser?.credentials.accessToken
            let url = "\(currentUser.photoUrl ?? "")?oauth_token=\(accessToken ?? "")"

            userProfilePicture.kf.setImage(with: URL(string: url)!, placeholder: UIImage(named: "ProfileEx"), options: nil, progressBlock: nil, completionHandler: { (img, err , cache, nil) in
                if let _ = img{
                    self.profilePictureLoader.stopAnimating()
                    self.profilePictureLoader.isHidden = true
                }
            })
        }
    }
}
