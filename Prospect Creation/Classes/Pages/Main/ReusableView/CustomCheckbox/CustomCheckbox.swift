//
//  CustomCheckbox.swift
//  Prospect Creation
//
//  Created by elie buff on 11/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

protocol CustomCheckBoxDelegate {
    func itemSelected(selectedValue: AnyObject?)
}

class CustomCheckbox: UIView {
    var contentView : UIView?
    var value : AnyObject?
    
    var delegate: CustomCheckBoxDelegate?
    
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var selectedImage: UIImageView!
    @IBOutlet weak var typeImage: UIImageView!
   
    
    var IsSelected: Bool? = nil {
        didSet {
            if let IsSelected = IsSelected{
                selectedImage.image = IsSelected ? #imageLiteral(resourceName: "selectedCheckbox") : #imageLiteral(resourceName: "nonSelectedCheckbox")
            }
        }
    }
    
    @IBInspectable var Label: String? = nil {
        didSet {
            if let label = Label{
                titleLbl.text = label
            }
        }
    }
    
    @IBInspectable var ImageType: UIImage? = nil {
        didSet {
            if let imageType = ImageType{
                typeImage.image = imageType
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
        
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    @IBAction func selectCheckBox(_ sender: Any) {
        if let delegate = delegate{
            delegate.itemSelected(selectedValue: value)
        }
    }
}
