//
//  FlagButtonsView.swift
//  Prospect Creation
//
//  Created by elie buff on 12/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

protocol LanguageSelectionDelegate{
    func languageSelected(selectedLang: String?)
}


class FlagButtonsView: UIView {
    let LAN_CODE = ["fr", "en", "zh"]
    
    var contentView : UIView?
    var delegate: LanguageSelectionDelegate?
    
    @IBOutlet var flagButtonItems:[ FlagButtonView]!
    
    var selectedLang: String?{
        didSet{
            setSelectedLanguage()
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
    }
    
    func loadViewFromNib() -> UIView! {
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        initView();
        return view
    }
    
    func initView(){
        for (index, item) in flagButtonItems.enumerated(){
            item.selectedLanguageCode = LAN_CODE[index]
            item.onFlagButtonCLick = onFlagButtonCLick
        }
    }
    
    func setSelectedLanguage(){
        for (index, item) in flagButtonItems.enumerated(){
            if selectedLang == LAN_CODE[index]{
                item.isSelected = true
            }else{
                item.isSelected = false
            }
        }
    }
    
    func onFlagButtonCLick(selectedLanguageCode: String){
        if let delegate = delegate{
            delegate.languageSelected(selectedLang: selectedLanguageCode)
        }
    }
}

