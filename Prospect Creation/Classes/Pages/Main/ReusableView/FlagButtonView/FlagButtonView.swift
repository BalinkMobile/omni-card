//
//  FlagButtonView.swift
//  Prospect Creation
//
//  Created by elie buff on 08/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class FlagButtonView: UIView {
    var contentView : UIView?
    var onFlagButtonCLick: ((String) -> Void)?
   
    @IBOutlet weak var flagButton: UIButton!
    @IBOutlet weak var isSelectedBar: UIView!
    
    var selectedLanguageCode: String?{
        didSet{
            if let selectedLanguageCode = selectedLanguageCode{
                flagButton.setImage(UIImage(named: selectedLanguageCode), for: .normal)
            }
        }
    }
    
    var isSelected: Bool?{
        didSet{
            if let isSelected = isSelected{
                if isSelected {
                    isSelectedBar.fadeIn(duration: 0.2)
                } else {
                    isSelectedBar.fadeOut(0.2)
                }
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }
    
    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
        
        isSelectedBar.alpha = 0
        flagButton.addTarget(self, action: #selector(onButtonCick), for: .touchUpInside)
    }
    
    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }
    
    @objc func onButtonCick(sender: UIButton!) {
        if let onFlagButtonCLick = onFlagButtonCLick{
            onFlagButtonCLick(selectedLanguageCode ?? "")
        }
    }
}

