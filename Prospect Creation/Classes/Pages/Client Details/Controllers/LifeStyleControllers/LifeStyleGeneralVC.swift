//
//  LifeStyleGeneralVC.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 19/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//


import UIKit
import ReSwift
import CoreData

class LifeStyleGeneralVC: ParentFormVC{
    var client: ClientWrapper?
    
    override func viewDidLayoutSubviews(){
        self.initForm()
    }
    
    @IBAction func done(_ sender: Any) {
        ClientDetailsUtils.updateClientDetails(client:self.client!.updateWrapper(map: resultMap) )
            .done{ (request) -> Void in
                self.navigationController?.popViewController(animated: true)
        }
    }
}

extension LifeStyleGeneralVC : StoreSubscriber{
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        mainStore.subscribe(self){ subcription in
            subcription.select { state in state.clientDetailState }
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        mainStore.unsubscribe(self)
    }
    
    func newState(state: ClientDetailState) {
        if let currentClient = state.selectedClient{
            self.client = currentClient
            createForm()
        }
    }
    
    func createForm(){
        self.sectionsArray = [""]
        
        let fields = [
            (label: "Loewe Projects", fieldName: "gender", type: FormFieldType.Picklist, options: ["objectName": "Contact" as AnyObject, "fieldName" : "b2c_fld_gender" as AnyObject], value: self.client?.gender as AnyObject),
            (label: "Hobbies", fieldName: "gender", type: FormFieldType.Picklist, options: ["objectName": "Contact" as AnyObject, "fieldName" : "b2c_fld_gender" as AnyObject], value: self.client?.gender as AnyObject),
            (label: "Brands I like", fieldName: "gender", type: FormFieldType.Picklist, options: ["objectName": "Contact" as AnyObject, "fieldName" : "b2c_fld_gender" as AnyObject], value: self.client?.gender as AnyObject)
        ] as [(label:String, fieldName:String, type: FormFieldType, options:[String : AnyObject]?, value: AnyObject?)]
        
        self.setFields[""] = fields
    }
}



