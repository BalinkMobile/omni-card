//
//  WebService.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import os
import CoreData
import SalesforceSDKCore
import SalesforceSwiftSDK
import ReSwift
import PromiseKit

class WebService{
    static let WS_ENDPOINT = Utils.getPlistValue(for: "WebServiceEndPoint") as? String ?? "/services/apexrest/"
    static let WS_ATTEMPT_NUMBER = 3
    
    static func SOQLExecuter(query:String, onCompleteBlock: ((_ records:[[String: AnyObject]]) -> ())? = nil)-> Promise<[[String: AnyObject]]>{
        var totalRecords = [[String: AnyObject]]()
        
        func checkNextRecord(_ sfRestResponse: SFRestResponse) -> Promise<[[String: AnyObject]]>{
            var restResonse = sfRestResponse.asJsonDictionary()
            if let records = restResonse["records"] as? [[String: AnyObject]] {
                if let onCompleteBlock = onCompleteBlock{
                    onCompleteBlock(records)
                    if let nextRecordsUrl = restResonse["nextRecordsUrl"] as? String{
                        return  getNextRecordsURL(nextRecordsUrl: nextRecordsUrl)
                    }
                }
                return  Promise(.pending) {  resolver in
                    resolver.fulfill(records)
                }
            }
            return Promise(.pending) {  resolver in
                resolver.fulfill([[String: AnyObject]]())
            }
        }
        
        func getNextRecordsURL(nextRecordsUrl :String) -> Promise<[[String: AnyObject]]> {
            
            let request = SFRestRequest(method: .GET, path: nextRecordsUrl, queryParams: nil)
            
            let restApi  = SFRestAPI.sharedInstance()
            let promiseReq =
                firstly{
                    restApi.Promises.send(request: request)
            }
            promiseReq
                .catch { error in
                    UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
            }
            
            return promiseReq
                .then { sfRestResponse  in
                    return checkNextRecord(sfRestResponse)
            }
        }
        
        
        func performSOQLQuery() -> Promise<[[String: AnyObject]]> {
            
            let restApi  = SFRestAPI.sharedInstance()
            let promiseReq =
                firstly{
                    restApi.Promises.query(soql: query)
                    }.then { request in
                        restApi.Promises.send(request: request)
            }
            
            promiseReq
                .catch { error in
                    UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
            }
            
            return promiseReq
                .then { sfRestResponse  in
                    checkNextRecord(sfRestResponse)
            }
        }
        
        return performSOQLQuery()
        
    }
    
    static func SOSLExecuter( soslQuery:String){
        
        
        func performSOSLQuery(numOfTry :Int){
            /*
             var request = SFRestAPI.sharedInstance().request(forSearch: query)
             
             SFRestAPI.sharedInstance().send(request,fail: { (err, urlResp) in
             os_log(err as! StaticString, log: OSLog.default, type: .error)
             onFailure(CustomError(localizedTitle: "Error", localizedDescription: getErrorDescription(err: err), code: 1))
             }) { (response, urlResp) in
             if let  result = response as? [String:AnyObject], let results = result["searchRecords"] as? [[String:AnyObject]]{
             onSuccess(results)
             } else{
             onFailure(nil)
             }
             }*/
        }
        
        let restApi  = SFRestAPI.sharedInstance()
        restApi.Promises.search(sosl: soslQuery)
            .then { request in
                restApi.Promises.send(request: request)
            }
            .then { sfRestResponse in
                getRecordsPromise(sfRestResponse: sfRestResponse)
        }
    }
    
    
    static func delete(objectName:String, id:String) -> Promise<SFRestResponse>{
        let restApi  = SFRestAPI.sharedInstance()
        let promiseReq = restApi.Promises.delete(objectType: objectName, objectId: id)
            .then{ (request) in
                restApi.Promises.send(request: request)
        }
        
        promiseReq
            .catch { error in
                UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
        }
        
        return promiseReq
    }
    
    static func insert(objectName:String, fields:[String:AnyObject?]) -> Promise<SFRestResponse>{
        
        let restApi  = SFRestAPI.sharedInstance()
        let promiseReq = restApi.Promises.create(objectType: objectName, fields: fields)
            .then { (request) in
                restApi.Promises.send(request: request)
        }
        
        promiseReq
            .catch { error in
                UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
        }
        
        return promiseReq
    }
    
    
    static func update(objectName:String, objectId:String?, fields:[String:AnyObject]) -> Promise<SFRestResponse>{
        var newFields = fields
        newFields.removeValue(forKey: "Id")
        
        let restApi  = SFRestAPI.sharedInstance()
        let promiseReq = restApi.Promises.update(objectType: objectName, objectId: objectId!, fieldList: newFields, ifUnmodifiedSince: nil)
            .then { (request) in
                restApi.Promises.send(request: request)
        }
        
        promiseReq
            .catch { error in
                UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
        }
        
        return promiseReq
    }
    
    static func upsert(objectName:String, objectId:String?, fields:[String:AnyObject]) -> Promise<SFRestResponse>{
        
        if let objectId = objectId{
            return self.update(objectName: objectName, objectId: objectId, fields: fields)
        }
        else{
            return self.insert(objectName: objectName, fields: fields)
        }
    }
    
    static func ExecuteWS(restMethod: SFRestMethod, wsName :String, queryParams:[String: String]?, body:[String: AnyObject]?) ->Promise<SFRestResponse>{
        
        let request = SFRestRequest(method: restMethod, path: wsName, queryParams: queryParams)
        request.endpoint = self.WS_ENDPOINT
        
        if let body = body, restMethod == .POST{
            request.setCustomRequestBodyDictionary(body, contentType: "application/json")
        }
        
        let restApi  = SFRestAPI.sharedInstance()
        let sendPromise = restApi.Promises.send(request: request)
        
        sendPromise
            .catch { error in
                UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
        }
        
        return sendPromise
            .then { sfRestResponse in
                getResponsePromise(sfRestResponse: sfRestResponse)
        }
    }
    
    static func UploadUserProfile(imageData :Data,
                                  onFailure: @escaping (CustomError?)->Void,
                                  onComplete: @escaping (_ profileUrl : String?) -> ()){
        
        let apiVersion = SFRestAPI.sharedInstance().apiVersion
        let currentUserId = SFUserAccountManager.sharedInstance().currentUserIdentity?.userId
        
        
        let path = "/\(apiVersion)/connect/user-profiles/\(currentUserId)/photo"
        let params = [String : String]()
        
        
        let request = SFRestRequest(method: .POST, path: path, queryParams: params)
        request.addPostFileData(imageData, paramName: "", description: "fileUpload", fileName: "My Profile", mimeType: "image/jpeg")
        
        SFRestAPI.sharedInstance().send(request, fail: { (err, urlResp) in
            print(err ?? "")
            onFailure(CustomError(localizedTitle: "Error", localizedDescription: getErrorDescription(err: err), code: 1))
        }, complete: { (response, urlResp) in
            print(response!)
            if let response = response as? [String: Any]{
                onComplete(response["largePhotoUrl"] as! String?)
            }
        })
    }
    
    static func GetAttachmentBody(attachmentId :String,
                                  onFailure: @escaping (CustomError?)->Void,
                                  onComplete: @escaping (_ imageData : Any?) -> ()){
        
        let apiVersion = SFRestAPI.sharedInstance().apiVersion
        
        let path = "/\(apiVersion)/sobjects/Attachment/\(attachmentId)/body"
        
        let request = SFRestRequest(method: .GET, path: path, queryParams: nil)
        request.endpoint = "/services/data/"
        request.parseResponse = false
        SFRestAPI.sharedInstance().send(request, fail: { (err, urlResp) in
            print(err ?? "")
            onFailure(CustomError(localizedTitle: "Error", localizedDescription: getErrorDescription(err: err), code: 1))
        }){ (records, urlResponse) in
            onComplete(records)
        }
    }
    
    static func getRecordsPromise(sfRestResponse :SFRestResponse) -> Promise<[[String: AnyObject]]>{
        return  Promise(.pending) {  resolver in
            var restResonse = sfRestResponse.asJsonDictionary()
            guard let records = restResonse["records"] as? [[String: AnyObject]] else{
                return
            }
            resolver.fulfill(records)
        }
    }
    
    static func getResponsePromise(sfRestResponse :SFRestResponse) -> Promise<SFRestResponse>{
        return  Promise(.pending) {  resolver in
            resolver.fulfill(sfRestResponse)
        }
    }
    
    static func getStringPromise(text : String) -> Promise<String>{
        return  Promise(.pending) {  resolver in
            resolver.fulfill(text)
        }
    }
    
    
    static func getErrorDescription(err :Error?) ->String{
        var errorDescription = err?.localizedDescription ?? ""
        /*if let underlyingError = (err! as NSError).userInfo["NSUnderlyingError"] as? NSError{
         errorDescription = underlyingError.localizedDescription
         }*/
        
        if let underlyingErrors = (err! as NSError).userInfo["error"] as? [AnyObject], let underlyingError = underlyingErrors.first as? [String :AnyObject]{
            errorDescription = "\(underlyingError["errorCode"] as? String ?? "") - \(underlyingError["message"] as? String ?? "")"
        } else if let underlyingError = (err! as NSError).userInfo["error"] as? String{
            errorDescription = underlyingError
        }
        
        return errorDescription
    }
}
