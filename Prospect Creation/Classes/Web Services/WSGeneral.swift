//
//  WSGeneral.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit
import CoreData
import PromiseKit
import SalesforceSDKCore
import SalesforceSwiftSDK

protocol WSGeneral{
    static var SFWsClassName: String {get}
    static var salesforceObjectName:String {get}
    static var toSalesforceMapping:[String:String] {get}
}
extension WSGeneral{
    
    // MARK: Appointment Method
    static func ExecuteSOQlQuery(query: String) -> Promise<[[String: AnyObject]]>{
        return WebService.SOQLExecuter(query: query)
    }
    
    static func ExecuteSOSLQuery(query: String){
        WebService.SOSLExecuter(soslQuery: query)
    }
    
    static func upsertMaps(mapObjects: [[String:AnyObject?]]) -> Promise<SFRestResponse>{
        var body = [String: AnyObject] ()
        body["items"] = mapObjects as AnyObject
        body["objectName"] = salesforceObjectName as AnyObject
        
        let promiseReq = WebService.ExecuteWS(restMethod: .POST, wsName: "/\(SFWsClassName)", queryParams: nil, body: body)
        
        promiseReq.catch { error in
            UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
        }
        
        return promiseReq
    }
    
    static func delete(id: String) -> Promise<SFRestResponse>{
        var query = [String: String] ()
        query["id"] = id
        query["objectName"] = salesforceObjectName
        
        let promiseReq = WebService.ExecuteWS(restMethod: .DELETE, wsName: "/\(SFWsClassName)", queryParams: query, body: nil)
        
        promiseReq.catch { error in
            UIApplication.topViewController()?.checkWSError(error: (title: "Error", description: WebService.getErrorDescription(err: error)))
        }
        
        return promiseReq
    }
}

