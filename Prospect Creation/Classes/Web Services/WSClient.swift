//
//  WSClient.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 10/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation

class WSClient : WSGeneral{
    static var SFWsClassName = "clientDetails"
    static var salesforceObjectName = "Client.SFObjectName"
    static var toSalesforceMapping = [String:String]()
}

