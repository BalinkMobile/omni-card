//
//  PickListObject+CoreDataClass.swift
//  IOS Template
//
//  Created by Jeremy Martiano on 22/04/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//


import Foundation
import CoreData


public class PickListObject: NSManagedObject, NSManagedObjectParent {
    static var SFObjectName = ""
    static var entityName: String = "PickListObject"
    
    static func getValues(for field:String, in object:String, with controllingFieldValue: String?, availableValues: [String]? = nil) -> [(label :String, value :String)]?
    {
        let context = CoreDataUtils.sharedInstance.context
        let valuesFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        var subpredicates = [NSPredicate]();
        subpredicates.append(NSPredicate(format:"objectName = %@", object))
        subpredicates.append(NSPredicate(format:"fieldName = %@", field))
        
        if let controllingFieldValue = controllingFieldValue{
            subpredicates.append(NSPredicate(format:"controllingFieldValue = %@", controllingFieldValue))
        }
        
        let andPredicate = NSCompoundPredicate(type: NSCompoundPredicate.LogicalType.and, subpredicates: subpredicates)
        
        valuesFetch.predicate = andPredicate
        
        do {
            let fetchedPicklists = try context?.fetch(valuesFetch) as! [PickListObject]?
            if let picklistValues = fetchedPicklists, picklistValues.count > 0{
                var result = [(label :String, value :String)]()
                
                let values = picklistValues[0].values?.components(separatedBy: ";")
                let labels = picklistValues[0].labels?.components(separatedBy: ";")
                
                if let values = values, let labels = labels{
                    for (index, _) in values.enumerated(){
                        let value = values[index]
                        if let availableValues = availableValues{
                            if availableValues.contains(value){
                                result.append((label :labels[index], value :value))
                            }
                        }else{
                            result.append((label :labels[index], value :value))
                        }
                    }
                }
                
                return result
            }
        } catch {
            fatalError("Failed to fetch tasks: \(error)")
        }
        return nil
    }
}

