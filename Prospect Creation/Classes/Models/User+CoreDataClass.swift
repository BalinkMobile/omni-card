//
//  User+CoreDataClass.swift
//  Clientleling Template
//
//  Created by elie buff on 05/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


public class User: NSManagedObject, NSManagedObjectParent {
    static var SFObjectName = "User"
    static var entityName: String = "User"
}
