//
//  Mapping+CoreDataProperties.swift
//  Clientleling Template
//
//  Created by elie buff on 09/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension Mapping {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Mapping> {
        return NSFetchRequest<Mapping>(entityName: "Mapping")
    }

    @NSManaged public var objectName: String?
    @NSManaged public var sfFieldName: String?
    @NSManaged public var wrapperFieldName: String?
    @NSManaged public var id: String?

}
