//
//  Client+CoreDataProperties.swift
//  Clientleling Template
//
//  Created by Nehora Sharabi on 02/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension Client {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Client> {
        return NSFetchRequest<Client>(entityName: "Client")
    }

    @NSManaged public var apt: Double
    @NSManaged public var birthdayDate: NSDate?
    @NSManaged public var city: String?
    @NSManaged public var clientType: String?
    @NSManaged public var country: String?
    @NSManaged public var countryName: String?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var email: String?
    @NSManaged public var favorite: Bool
    @NSManaged public var firstName: String?
    @NSManaged public var firstPurchaseDate: NSDate?
    @NSManaged public var gender: String?
    @NSManaged public var homePhone: String?
    @NSManaged public var id: String?
    @NSManaged public var lastContactedDate: NSDate?
    @NSManaged public var lastModifiedDate: NSDate?
    @NSManaged public var lastName: String?
    @NSManaged public var lastPurchaseDate: NSDate?
    @NSManaged public var mobile: String?
    @NSManaged public var nbOnlineTransactions: Int16
    @NSManaged public var nbShopTransactions: Int16
    @NSManaged public var newsletterOptIn: Bool
    @NSManaged public var optInEmails: Bool
    @NSManaged public var optInPhone: Bool
    @NSManaged public var optInPostal: Bool
    @NSManaged public var ownerId: String?
    @NSManaged public var ownerName: String?
    @NSManaged public var preferredContactChannel: String?
    @NSManaged public var segmentation: String?
    @NSManaged public var state: String?
    @NSManaged public var storeId: String?
    @NSManaged public var storeName: String?
    @NSManaged public var street: String?
    @NSManaged public var turnover: Double
    @NSManaged public var upt: Double
    @NSManaged public var zipCode: String?
    @NSManaged public var clientLists: ClientList?

}
