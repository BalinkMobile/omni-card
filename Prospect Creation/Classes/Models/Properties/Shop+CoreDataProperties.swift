//
//  Shop+CoreDataProperties.swift
//  Clientleling Template
//
//  Created by elie buff on 05/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension Shop {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Shop> {
        return NSFetchRequest<Shop>(entityName: "Shop")
    }

    @NSManaged public var name: String?
    @NSManaged public var id: String?
    @NSManaged public var storeCode: String?

}
