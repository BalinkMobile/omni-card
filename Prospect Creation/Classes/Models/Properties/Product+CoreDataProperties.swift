//
//  Product+CoreDataProperties.swift
//  Clientleling Template
//
//  Created by Shlomo Ariel on 15/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension Product {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<Product> {
        return NSFetchRequest<Product>(entityName: "Product")
    }
    
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    
}

// MARK: Generated accessors for stones
extension Product {
    
    
    @objc(addStones:)
    @NSManaged public func addToStones(_ values: NSSet)
    
    @objc(removeStones:)
    @NSManaged public func removeFromStones(_ values: NSSet)
    
}
