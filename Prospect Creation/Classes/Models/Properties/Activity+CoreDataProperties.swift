//
//  Activity+CoreDataProperties.swift
//  Clientleling Template
//
//  Created by elie buff on 16/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension Activity {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Activity> {
        return NSFetchRequest<Activity>(entityName: "Activity")
    }

    @NSManaged public var activityDate: NSDate?
    @NSManaged public var activityType: String?
    @NSManaged public var clientFirstName: String?
    @NSManaged public var clientId: String?
    @NSManaged public var clientLastName: String?
    @NSManaged public var createdUserId: String?
    @NSManaged public var desc: String?
    @NSManaged public var endDateTime: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var location: String?
    @NSManaged public var outreachType: String?
    @NSManaged public var ownerFirstName: String?
    @NSManaged public var ownerId: String?
    @NSManaged public var ownerLastName: String?
    @NSManaged public var priority: String?
    @NSManaged public var startDateTime: NSDate?
    @NSManaged public var subject: String?
    @NSManaged public var status: String?

}
