//
//  CacheObject+CoreDataProperties.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 05/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension CacheObject {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CacheObject> {
        return NSFetchRequest<CacheObject>(entityName: "CacheObject")
    }

    @NSManaged public var createdDate: NSDate?
    @NSManaged public var id: String?
    @NSManaged public var json: String?
    @NSManaged public var type: String?

}
