//
//  ClientList+CoreDataProperties.swift
//  
//
//  Created by Shlomo Ariel on 30/07/2018.
//
//

import Foundation
import CoreData


extension ClientList {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<ClientList> {
        return NSFetchRequest<ClientList>(entityName: "ClientList")
    }

    @NSManaged public var id: String?
    @NSManaged public var createdDate: NSDate?
    @NSManaged public var listDescription: String?
    @NSManaged public var isPermanent: Bool
    @NSManaged public var position: Int16
    @NSManaged public var type: String?
    @NSManaged public var name: String?
    @NSManaged public var clients: NSSet?

}
