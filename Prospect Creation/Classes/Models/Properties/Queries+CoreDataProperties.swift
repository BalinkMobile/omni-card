//
//  Queries+CoreDataProperties.swift
//  IOS Template
//
//  Created by Jeremy Martiano on 02/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension Queries {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Queries> {
        return NSFetchRequest<Queries>(entityName: "Queries")
    }

    @NSManaged public var name: String?
    @NSManaged public var query: String?
    @NSManaged public var id: String?

}
