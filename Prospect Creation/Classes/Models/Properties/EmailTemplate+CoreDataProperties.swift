//
//  EmailTemplate+CoreDataProperties.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 19/08/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
//

import Foundation
import CoreData


extension EmailTemplate {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<EmailTemplate> {
        return NSFetchRequest<EmailTemplate>(entityName: "EmailTemplate")
    }

    @NSManaged public var body: String?
    @NSManaged public var id: String?
    @NSManaged public var lang: String?
    @NSManaged public var name: String?
    @NSManaged public var subject: String?
    @NSManaged public var type: String?

}
