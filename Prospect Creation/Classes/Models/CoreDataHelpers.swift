//
//  Utils.swift
//  Clientleling Template
//
//  Created by elie buff on 05/08/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation
import CoreData

public class CoreDataHelpers{
    
    static func getClientSearchPredicate(forText searchText: String) -> NSPredicate?{
        
        if searchText != ""{
            var predicates = [NSPredicate]()
            
            let searchArray = searchText.components(separatedBy: " ")
            
            for text in searchArray{
                predicates.append(NSPredicate(format: "firstName contains[cd] %@", text))
                predicates.append(NSPredicate(format: "lastName contains[cd] %@", text))
            }
            predicates.append(NSPredicate(format: "email contains[cd] %@", searchText))
            predicates.append(NSPredicate(format: "mobile contains[cd] %@", searchText))
            predicates.append(NSPredicate(format: "homePhone contains[cd] %@", searchText))
            
            return NSCompoundPredicate(orPredicateWithSubpredicates: predicates)
        }
        return nil
    }
    
    static func convertToGenericList(item: NSManagedObject?) -> (label:String, value: String){
        guard let item = item else{
            return (label:"", value: "")
        }
        
        let value = item.value(forKey: "id") as? String ?? ""
        let label = item.value(forKey: "name") as? String ?? ""
        return (label:label, value: value)
        
    }
    
    static func convertToGenericList(items: [NSManagedObject]?) -> [(label:String, value: String)]{
        guard let items = items else{
            return [(label:"", value: "")]
        }
        var result = [(label:String, value: String)]()
        
        for item in items{
            result.append(convertToGenericList(item: item))
        }
        
        return result
        
    }
}
