//
//  CustomDatePopover.swift
//  Prospect Creation
//
//  Created by elie buff on 24/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class CustomDatePopover: UIViewController, CustomDatePickerDelegate {
    @IBOutlet weak var customDatePicker: CustomDatePicker!
    
    var selectedDate : (day: Int?, month: Int?, year: Int?)?
    var delegate:PopPickerDelegate?
    var maxYear: Int?
    
    var label: String?
    var value: [Int?]?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        customDatePicker.datePickerDelegate = self
        customDatePicker.maxYear = maxYear
        customDatePicker.selectedDate = self.selectedDate
        
        
    }
    
    func selectedDateChange(label: String, value: [Int?]){
        self.label = label
        self.value = value
    }
    
    @IBAction func okButtonClick(_ sender: AnyObject) {
        self.dismiss(animated: true) { () -> Void in
            self.delegate!.popoverValueChange(label: self.label ?? "", value: self.value as AnyObject)
        }
    }
    
    @IBAction func cancelButtonClick(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}
