//
//  PopoverManager.swift
//  Prospect Creation
//
//  Created by elie buff on 09/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
import Foundation
import UIKit

protocol PopPickerDelegate : class {
    func popoverValueChange(label: String, value: AnyObject)
}

class PopPickerManager :NSObject, UIPopoverPresentationControllerDelegate{
    public typealias PopPickerCallback = (_ label: String, _ value: AnyObject)->()
    
    
    var datePopoverVC : DatePopover
    var customDatePopoverVC : CustomDatePopover
    var listPopoverVC : PicklistPopover
    var tablePopoverVC : SelectTableViewVC
    
    var textField : UITextField!
    var button : UIButton?
    var presented = false
    var dataChanged : PopPickerCallback?
    
    
    init(srcTextField: UITextField) {
        
        listPopoverVC = PicklistPopover()
        datePopoverVC = DatePopover()
        customDatePopoverVC = CustomDatePopover()
        tablePopoverVC = SelectTableViewVC()
        
        self.textField = srcTextField
        super.init()
    }
    
    init(srcButton: UIButton){
        listPopoverVC = PicklistPopover()
        datePopoverVC = DatePopover()
        customDatePopoverVC = CustomDatePopover()
        tablePopoverVC = SelectTableViewVC()
        self.button = srcButton
        super.init()
    }
    
    func pickList(_ inViewController : UIViewController, width : CGFloat, popOverDir : UIPopoverArrowDirection, values : [(label:String, value:String)]?, selectedValue: (label:String, value:String)?,setSearchBar: Bool = false, dataChanged : @escaping PopPickerCallback) {
        
        if presented || values == nil{
            return
        }
        
        var selectedTitle = (label:"", value:"")
        if let selectedValue = selectedValue{
            selectedTitle = selectedValue
        }else if values!.count > 0{
            selectedTitle = values!.first!
        }
        
        listPopoverVC.selectecdItem = selectedTitle
        listPopoverVC.delegate = self
        listPopoverVC.items = values ?? []
        listPopoverVC.setSearchBar = setSearchBar
        
        
        listPopoverVC.modalPresentationStyle = UIModalPresentationStyle.popover
        listPopoverVC.preferredContentSize = CGSize(width: width, height: 245)
        
        let popover = listPopoverVC.popoverPresentationController
        if let popover = popover {
            
            popover.sourceView = textField
            popover.sourceRect = CGRect(x: textField.bounds.size.width/2,y: 0, width: 0, height: 0)
            popover.delegate = self
            popover.permittedArrowDirections = popOverDir
            self.dataChanged = dataChanged
            inViewController.present(listPopoverVC, animated: true, completion: nil)
            presented = true
        }
    }
    
    func pickDate(_ inViewController : UIViewController, width : CGFloat, popOverDir : UIPopoverArrowDirection, selectedDate: Date?, dataChanged : @escaping PopPickerCallback) {
        self.dataChanged = dataChanged
        
        datePopoverVC.selectedDate = selectedDate
        datePopoverVC.delegate = self
        datePopoverVC.modalPresentationStyle = UIModalPresentationStyle.popover
        datePopoverVC.preferredContentSize = CGSize(width: width, height: 220)
        
        let popover = datePopoverVC.popoverPresentationController
        if let popover = popover {
            
            popover.sourceView = textField
            popover.sourceRect = CGRect(x: textField.bounds.size.width/2, y: 0, width: 0, height: 0)
            popover.delegate = self
            popover.permittedArrowDirections = popOverDir
            
            inViewController.present(datePopoverVC, animated: true, completion: nil)
            presented = true
        }
    }
    
    func customPickDate(_ inViewController : UIViewController, width : CGFloat, popOverDir : UIPopoverArrowDirection, selectedDate: (day: Int?, month: Int?, year: Int?)?, maxYear: Int?, dataChanged : @escaping PopPickerCallback) {
        self.dataChanged = dataChanged
        
        
        customDatePopoverVC.delegate = self
        customDatePopoverVC.maxYear = maxYear
        customDatePopoverVC.selectedDate = selectedDate
        customDatePopoverVC.modalPresentationStyle = UIModalPresentationStyle.popover
        customDatePopoverVC.preferredContentSize = CGSize(width: width, height: 220)
        
        
        let popover = customDatePopoverVC.popoverPresentationController
        if let popover = popover {
            
            popover.sourceView = textField
            popover.sourceRect = CGRect(x: textField.bounds.size.width/2,y: 0,width: 0,height: 0)
            popover.delegate = self
            popover.permittedArrowDirections = popOverDir
            
            inViewController.present(customDatePopoverVC, animated: true, completion: nil)
            presented = true
        }
    }
    
    func popTable(_ inViewController : UIViewController, width : CGFloat, popOverDir : UIPopoverArrowDirection, values : [(label:String, value:String)]?, selectedValue: (label:String, value:String)?, dataChanged : @escaping PopPickerCallback) {
        
        if presented || values == nil{
            return
        }
        
        var selectedTitle = (label:"", value:"")
        if let selectedValue = selectedValue{
            selectedTitle = selectedValue
        }else if values!.count > 0{
            selectedTitle = values!.first!
        }
        
        tablePopoverVC.selectedItem = selectedTitle
        tablePopoverVC.delegate = self
        tablePopoverVC.items = values ?? []
        let navigationController = UINavigationController(rootViewController: tablePopoverVC)
        navigationController.navigationBar.barTintColor = .white
        navigationController.navigationBar.titleTextAttributes = [.foregroundColor: #colorLiteral(red: 0.2666666667, green: 0.2666666667, blue: 0.2666666667, alpha: 1)]
        navigationController.navigationBar.tintColor = #colorLiteral(red: 0.2666666667, green: 0.2666666667, blue: 0.2666666667, alpha: 1)
        
        navigationController.modalPresentationStyle = UIModalPresentationStyle.popover
        navigationController.preferredContentSize = CGSize(width: width, height: 330)
        
        let popover = navigationController.popoverPresentationController
        if let popover = popover {
            
            popover.sourceView = button ?? textField
            popover.sourceRect = CGRect(x: (popover.sourceView?.bounds.size.width ?? 0)/2,y: popover.sourceView?.bounds.size.height ?? 0,width: 0,height: 0)
            popover.delegate = self
            popover.permittedArrowDirections = popOverDir
            self.dataChanged = dataChanged
            
            inViewController.present(navigationController, animated: true, completion: nil)
            presented = true
        }
    }
}

extension PopPickerManager : PopPickerDelegate{
    func popoverValueChange(label: String, value: AnyObject){
        if let dataChanged = dataChanged{
            dataChanged(label, value)
        }
    }
}

