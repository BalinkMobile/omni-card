//
//  PicklistPopover.swift
//  Prospect Creation
//
//  Created by elie buff on 10/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class PicklistPopover: UIViewController {
    
    var delegate:PopPickerDelegate?
    var items = [(label:String, value:String)]()
    var filteredItems = [(label:String, value:String)]()
    var selectecdItem: (label:String, value:String)?
    var setSearchBar: Bool = false
    
    @IBOutlet weak var searchFieldHeight: NSLayoutConstraint!
    @IBOutlet weak var dataUIPicker: UIPickerView!
    @IBOutlet weak var searchField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        filteredItems = items
        if setSearchBar{
            searchFieldHeight.constant = 30
            searchField.withImageName(imageName: "searchIcon")
            searchField.clearButtonMode = .always
        } else {
            searchFieldHeight.constant = 0
        }
        
    }
    
    @IBAction func okButtonClick(_ sender: AnyObject) {
        self.dismiss(animated: true) { () -> Void in
            let itemValue = self.selectecdItem?.value ?? ""
            self.delegate!.popoverValueChange(label: self.selectecdItem?.label ?? "", value: itemValue as AnyObject)
        }
    }

    @IBAction func searchChanged(_ sender: UITextField) {
        if let searchText = sender.text, searchText != ""{
            filteredItems = items.filter({ (item) -> Bool in
                return item.label.lowercased().contains(find: searchText.lowercased())
            })
        } else {
            filteredItems = items
        }
        if filteredItems.count > 0{
            dataUIPicker.selectRow(0, inComponent: 0, animated: false)
            self.selectecdItem = filteredItems[dataUIPicker.selectedRow(inComponent: 0)]
        }else{
            self.selectecdItem = (label:"", value:"")
        }
         
        dataUIPicker.reloadAllComponents()
    }
    
    @IBAction func cancelButtonClick(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension PicklistPopover : UIPickerViewDataSource, UIPickerViewDelegate{
    func numberOfComponents(in pickerView: UIPickerView) -> Int{
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int{
        return filteredItems.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?{
        return filteredItems[row].label
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        pickerView.reloadComponent(0)
        
        if filteredItems.count > row{
            self.selectecdItem = self.filteredItems[row]
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        let selectedColor = #colorLiteral(red: 0.4509803922, green: 0.4509803922, blue: 0.4509803922, alpha: 1)
        let nonSelectedColor = #colorLiteral(red: 0.6156862745, green: 0.6156862745, blue: 0.6156862745, alpha: 1)
        let darkWhite = UIColor(red: 251/255, green: 251/255, blue: 251/255, alpha: 1)
        
        let selectedFont = UIFont(name: ".SFUIText", size: 16)
        let regularFont = UIFont(name: ".SFUIText", size: 14)
        
        let color = (row == pickerView.selectedRow(inComponent: component)) ? selectedColor : nonSelectedColor
        let font = (row == pickerView.selectedRow(inComponent: component)) ? selectedFont : regularFont
        let backgroundColor = (row == pickerView.selectedRow(inComponent: component)) ? UIColor.white : darkWhite
        
        var pickerLabel = view as! UILabel!
        if view == nil {  //if no label there yet
            pickerLabel = UILabel()
        }
        
        let titleData = filteredItems[row].label
        
        let myTitle = NSAttributedString(string: titleData, attributes: [NSAttributedStringKey.foregroundColor: color, NSAttributedStringKey.font: font!])
        pickerLabel?.attributedText = myTitle
        pickerLabel?.backgroundColor = backgroundColor
        pickerLabel?.textAlignment = .center
        return pickerLabel!
    }
    
    func pickerView(_ pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 40
    }
}
