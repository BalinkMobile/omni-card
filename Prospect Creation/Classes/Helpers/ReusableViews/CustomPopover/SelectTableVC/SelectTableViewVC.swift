//
//  SelectLanguageVC.swift
//  Prospect Creation
//
//  Created by Jeremy Martiano on 11/12/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

class SelectTableViewVC: UITableViewController {
    var items = [(label: String, value: String)]()
    var selectedItem: (label: String, value: String)?
    var delegate:PopPickerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let cellNib = UINib(nibName: "SelectSimpleTableCell", bundle: nil)
        tableView.register(cellNib, forCellReuseIdentifier: "selectCell")
        tableView.tableFooterView = UIView()
        
        let btn1 = UIButton(type: .custom)
        btn1.setTitle("Done", for: .normal)
        btn1.setTitleColor(#colorLiteral(red: 0.6431372549, green: 0.5058823529, blue: 0.337254902, alpha: 1), for: .normal)
        btn1.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        btn1.addTarget(self, action: #selector(SelectTableViewVC.okButtonClick), for: .touchUpInside)
        let item1 = UIBarButtonItem(customView: btn1)
        
        self.navigationItem.setRightBarButtonItems([item1], animated: true)
        self.navigationItem.title = "LANGUAGES"
    }
    
    @IBAction func okButtonClick(_ sender: AnyObject) {
        self.dismiss(animated: true) { () -> Void in
            self.delegate!.popoverValueChange(label: self.selectedItem?.label ?? "", value: (self.selectedItem?.value ?? "") as AnyObject)
        }
    }
    

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "selectCell", for: indexPath) as! SelectSimpleTableCell
        cell.titleLbl.text = items[indexPath.row].label
        cell.checkImg.image = items[indexPath.row] == selectedItem ?? (label: "", value: "") ?  #imageLiteral(resourceName: "selectedCheckbox") : #imageLiteral(resourceName: "nonSelectedCheckbox")
        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        selectedItem = items[indexPath.row]
        tableView.reloadData()
        
    }

}
