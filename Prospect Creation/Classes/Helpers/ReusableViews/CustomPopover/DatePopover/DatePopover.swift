//
//  DatePopover.swift
//  Prospect Creation
//
//  Created by elie buff on 09/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//
import UIKit

class DatePopover: UIViewController{
    @IBOutlet weak var datePicker: UIDatePicker!
    
    var selectedDate : Date?
    var delegate:PopPickerDelegate?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let grayColor = #colorLiteral(red: 0.4509803922, green: 0.4509803922, blue: 0.4509803922, alpha: 1)
        datePicker.date = selectedDate ?? Date()
        datePicker.setValue(grayColor, forKeyPath: "textColor");
    }
    
    @IBAction func okButtonClick(_ sender: AnyObject) {
        self.dismiss(animated: true) { () -> Void in
            let currentDate =  self.datePicker.date
            self.delegate!.popoverValueChange(label: "", value: currentDate as AnyObject)
        }
    }
    
    @IBAction func cancelButtonClick(_ sender: AnyObject) {
        self.dismiss(animated: true, completion: nil)
    }
}

