//
//  RoundedImage.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 12/08/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit


@IBDesignable class RoundedImage: UIImageView {
    

    @IBInspectable var CornerRadius: Int = 0 {
        didSet {
            self.layer.cornerRadius = CGFloat(CornerRadius)
            self.layer.masksToBounds = true
        }
    }
    
    @IBInspectable var Circle: Bool = false {
        didSet {
            if Circle {
                self.layer.cornerRadius = (self.frame.width / 2)
                self.layer.masksToBounds = true
            }
        }
    }

    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    @IBInspectable var borderColor: UIColor? = UIColor.clear {
        didSet {
            layer.borderColor = borderColor?.cgColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
    
}
