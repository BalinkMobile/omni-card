//
//  CustomDatePicker.swift
//  Prospect Creation
//
//  Created by elie buff on 24/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

protocol CustomDatePickerDelegate : class {
    func selectedDateChange(label: String, value: [Int?])
}

class CustomDatePicker:UIView{
    @IBOutlet weak var pickerView: UIPickerView!
    
    
    

    let monthComponents = Calendar.current.monthSymbols
    let currentDay = Date()
    let anchorComponents = Calendar.current.dateComponents([.day, .month, .year, .hour], from: Date())
    
    var datePickerDelegate : CustomDatePickerDelegate?
    var numOfYear = 100
    var maxYear: Int?
    var contentView : UIView?
    
    var selectedDate: (day: Int?, month: Int?, year: Int?)?{
        didSet{
            if let selectDate = selectedDate{
                self.selectedDay = selectedDate?.day ?? anchorComponents.day
                self.selectedMonth = selectedDate?.month ?? anchorComponents.month
                self.selectedYear = selectedDate?.year ?? anchorComponents.year
                
                setSelectedDate(selectedDay: self.selectedDay, selectedMonth: self.selectedMonth, selectedYear: self.selectedYear)
            }
        }
        
    }
    
    var selectedDay:Int?{
        didSet{
            var dayRow = 0
            if let selectedDay = selectedDay{
                dayRow = selectedDay - 1
            }
            self.pickerView.selectRow( dayRow, inComponent: 0, animated: true)
        }
    }
    var selectedMonth:Int?{
        didSet{
            var monthRow = 0
            if let selectedMonth = selectedMonth{
                monthRow = selectedMonth - 1
            }
            self.pickerView.selectRow(monthRow, inComponent: 1, animated: true)
        }
    }
    var selectedYear:Int?{
        didSet{
            var yearRow = numOfYear - 1
            if let selectedYear = selectedYear, let year = anchorComponents.year{
                 yearRow = numOfYear + (selectedYear - (maxYear ?? year)) - 2
                
            }
            self.pickerView.selectRow(yearRow , inComponent: 2, animated: true)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)
        xibSetup()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        xibSetup()
    }

    func xibSetup() {
        contentView = loadViewFromNib()
        // use bounds not frame or it'll be offset
        contentView!.frame = bounds
        
        // Make the view stretch with containing view
        contentView!.autoresizingMask = [UIViewAutoresizing.flexibleWidth, UIViewAutoresizing.flexibleHeight]
        
        // Adding custom subview on top of our view (over any custom drawing > see note below)
        addSubview(contentView!)
        
    }

    func loadViewFromNib() -> UIView! {
        
        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil)[0] as! UIView
        
        return view
    }

}

extension CustomDatePicker : UIPickerViewDelegate, UIPickerViewDataSource{
    
    func pickerView(_ pickerView: UIPickerView, widthForComponent component: Int) -> CGFloat {
        if component == 0{
            return 50
        }
        if component == 2{
            return 100
        }
        
        return 150
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 3
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0{
            return 31
        }else if component == 1{
            return monthComponents.count
        }
        
        return numOfYear
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        if component == 0{
            return String(row + 1)
        }
        if component == 1{
            return monthComponents[row]
        }
        else{
            let currentYear = (maxYear ?? anchorComponents.year ?? 1900)
            if(row == numOfYear-1){
                return "----"
            }
            return String(currentYear - numOfYear + row + 2)
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        if component == 0{
            selectedDay = row + 1
        }
        else if component == 1{
            selectedMonth = row + 1
        }
        else{
            if(row == numOfYear-1){
                selectedYear = nil
            } else {
                let currentYear = (maxYear ?? anchorComponents.year ?? 1900)
                selectedYear = currentYear - numOfYear + row + 2
            }
            
        }
        
        setSelectedDate(selectedDay: self.selectedDay, selectedMonth: self.selectedMonth, selectedYear: self.selectedYear)
    }
    
    func setSelectedDate(selectedDay : Int?, selectedMonth: Int?, selectedYear: Int?){
        if let datePickerDelegate = datePickerDelegate{
            let dayString = selectedDay == nil ? "_" : String(describing: selectedDay!)
            let monthString = selectedMonth == nil ? "_" : monthComponents[selectedMonth! - 1]
            let yearString = selectedYear == nil ? "_" : String(describing: selectedYear!)
            let dateLabel = "\(dayString) \(monthString) \(yearString)"
            let dateValues = [selectedDay, selectedMonth, selectedYear]
            
            datePickerDelegate.selectedDateChange(label: dateLabel, value: dateValues)
        }
    }
}
