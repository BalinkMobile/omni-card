//
//  RoundedView.swift
//  ICON
//
//  Created by Yossi Sud on 31/01/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import UIKit


@IBDesignable class RoundedView: UIView {
    var borderWidth: CGFloat = CGFloat(1.0)
    
    
    fileprivate var regularBackgroundColor : UIColor?
    
    @IBInspectable var ShadowColor: UIColor? = nil
    @IBInspectable var ShadowOffset: Int = 0
    @IBInspectable var ShadowRadius: Int = 0
    @IBInspectable var ShadowOpacity: Float = 0
    
    @IBInspectable var CornerRadius: Int = 0 {
        didSet {
            self.layer.cornerRadius = CGFloat(CornerRadius)
        }
    }
    
    @IBInspectable var RegularBackgroundColor: UIColor? = nil {
        didSet {
            if let color = RegularBackgroundColor{
                self.backgroundColor = color
                self.regularBackgroundColor = color
            }
        }
    }
    
    @IBInspectable var RegularBorderColor: UIColor? = nil {
        didSet {
            if let color = RegularBorderColor{
                self.layer.borderWidth = borderWidth
                self.layer.borderColor = color.cgColor
            }
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        initButton()
    }
    
    func initButton(){
        self.layer.shadowColor = ShadowColor?.cgColor
        self.layer.shadowOffset = CGSize(width: ShadowOffset, height: ShadowOffset)
        self.layer.shadowRadius = CGFloat(ShadowRadius)
        self.layer.shadowOpacity = ShadowOpacity
    }
}
