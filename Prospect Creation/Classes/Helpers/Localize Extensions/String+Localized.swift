//
//  String.swift
//  C. Everywhere
//
//  Created by Yossi Sud on 19/02/2017.
//  Copyright © 2017 Elie Buff. All rights reserved.
//

import Foundation

extension String {

    func localized(withComment comment:String) -> String {
        let translatedText =  NSLocalizedString(self, tableName: nil, bundle: LocalizeUtils.localizedBundle, value: "", comment: comment)
        
        if translatedText == ""{
            return self
        }
        return translatedText
    }
    
    func localized()-> String {
        let translatedText = NSLocalizedString(self, tableName: nil, bundle: LocalizeUtils.localizedBundle, value: "", comment: "")
        
        if translatedText == ""{
            return self
        }
        return translatedText
    }
}
