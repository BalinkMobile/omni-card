//
//  UISegmentedControl+LocalizedTitles.swift
//  C. Everywhere
//
//  Created by Yoni on 19/04/2017.
//  Copyright © 2017 Elie Buff. All rights reserved.
//

import UIKit

extension UISegmentedControl{
    open override func awakeFromNib() {
        super.awakeFromNib()
        for i in 0..<self.numberOfSegments{
            if let _title = self.titleForSegment(at: i), _title != "" {
                self.setTitle(_title.localized(), forSegmentAt: i)
            }
        }
        
        if let updatedFont = LocalizeHelper.setSpecialFont(currentFont: nil, currentSize: 16){
            self.setTitleTextAttributes([NSAttributedStringKey.font: updatedFont], for: .normal)
        }
    }
}
