//
//  UIView+FontLocalize.swift
//  ICON
//
//  Created by elie buff on 20/06/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

class LocalizeHelper{
    
    static let specialLocal = ["zh_TW", "zh_CN", "ko", "ja"]
    static let defaultFont = "HelveticaNeue"
    
    static func setSpecialFont(currentFont: String?, currentSize: CGFloat?) -> UIFont?{
        if specialLocal.contains(LocalizeUtils.localizedLocal){
           return UIFont(name: defaultFont, size: currentSize ?? 10)
        }
        return nil
    }
    
    /*static func setGeneralAppearanceSpecialFont(){
        if specialLocal.contains(LocalizeUtils.localizedLocal){
            UINavigationBar.appearance().titleTextAttributes = [NSForegroundColorAttributeName : UIColor.white, NSFontAttributeName: UIFont(name: defaultFont, size: 23.0) as Any]
            UIBarButtonItem.appearance().setTitleTextAttributes([NSFontAttributeName: UIFont(name: defaultFont, size: 17.0) as Any],for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : Colors.GOLD_LIGHT, NSFontAttributeName: UIFont(name: defaultFont, size: 13.0) as Any], for: .normal)
            UITabBarItem.appearance().setTitleTextAttributes([NSForegroundColorAttributeName : Colors.BROWN_DARK, NSFontAttributeName: UIFont(name: defaultFont, size: 13.0) as Any], for: .selected)

        }
    }*/
    
    static func getFamilyName(currentFamilyName: String, fontWeight: String?) ->String{
        var result = currentFamilyName
        if specialLocal.contains(LocalizeUtils.localizedLocal){
            result = defaultFont
        }
        
        if let fontWeight = fontWeight{
            result = result + "-\(fontWeight)"
        }
        
        
        return result;
    }
}
