//
//  UITextField+LocalizedText.swift
//  C. Everywhere
//
//  Created by Yoni on 18/04/2017.
//  Copyright © 2017 Elie Buff. All rights reserved.
//

import UIKit

extension UITextField {
    open override func awakeFromNib() {
        super.awakeFromNib()
        if let placeholderText = self.placeholder, placeholderText != ""{
            self.placeholder = placeholderText.localized()
        }
        
        if let updatedFont = LocalizeHelper.setSpecialFont(currentFont: font?.familyName, currentSize: font?.pointSize){
            self.font = updatedFont
        }
    }
}
