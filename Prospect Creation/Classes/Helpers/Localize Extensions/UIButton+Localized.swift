//
//  UIButton+LocalizedText.swift
//  C. Everywhere
//
//  Created by Yoni on 03/04/2017.
//  Copyright © 2017 Elie Buff. All rights reserved.
//

import UIKit

extension UIButton{
    open override func awakeFromNib() {
        super.awakeFromNib()
        if let title = self.currentTitle, title != "" {
            self.setTitle(title.localized(), for: .normal)
        }
       
        if let updatedFont = LocalizeHelper.setSpecialFont(currentFont: titleLabel?.font.familyName, currentSize: titleLabel?.font.pointSize){
            self.titleLabel?.font = updatedFont
        }
    }
}
