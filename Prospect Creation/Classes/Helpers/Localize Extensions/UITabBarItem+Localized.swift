//
//  UITabBarItem+LocalizedText.swift
//  C. Everywhere
//
//  Created by Yoni on 18/04/2017.
//  Copyright © 2017 Elie Buff. All rights reserved.
//

import UIKit

extension UITabBarItem{
    open override func awakeFromNib() {
        super.awakeFromNib()
        if let _title = self.title{
            self.title = _title.localized()
        }
        
    }
}
