//
//  NSManagedObjectParent.swift
//  Berluti
//
//  Created by elie buff on 12/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//


import CoreData

protocol NSManagedObjectParent {
    static var entityName: String {get}
    static var SFObjectName: String {get}
    
    static func getEntityMapping() ->[String:String]?
    static func executeBeforeDelete(_ managedObj :NSManagedObject)
    static func isReadyForDelete(_ managedObj :NSManagedObject) ->Bool
}

extension NSManagedObjectParent{
    static func getEntityMapping() ->[String:String]?{
        return [String:String]()
    }
    
    static func Upsert(_ records: [[String : Any]],
                       in context: NSManagedObjectContext,
                       deleteAllNotIn: Bool = false,
                       onItemUpsert: ((NSManagedObjectContext, NSManagedObject, [String : Any] ) -> Void)? = nil){
        context.performAndWait { () -> Void in
            var recordsValues = CoreDataUtils.getRecordsKeyValues(records, keys: ["id", "Id"])
            if recordsValues.count == 0{
                if deleteAllNotIn {
                    DeleteAllNotIN([])
                }
                return;
            }
            
            let recordValues = recordsValues["Id"]?.union(recordsValues["id"] ?? [])
            var itemMap = CoreDataUtils.findOrCreateItems(context, records: records, recordsValues: recordValues!, entity: entityName, attribute: "id")
            
            for var record in records {
                let itemId = (record["id"] != nil ? record["id"] : record["Id"]) as! String
                let item = itemMap[itemId]
                let managedObject = self.populateItem(record: record, item:item!!, mapping: getEntityMapping())
                if let onItemUpsert = onItemUpsert{
                    onItemUpsert(context, managedObject, record)
                }
            }
            CoreDataUtils.sharedInstance.saveContext(context: context, withParent: true)
            
            if let recordIds = recordValues, deleteAllNotIn{
                DeleteAllNotIN(recordIds)
            }
        }
    }
    
    
    static func DeleteAllNotIN(_ itemsIds: Set<String>){
        
        let context = CoreDataUtils.sharedInstance.context!
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: self.entityName)
        fetchRequest.predicate = NSPredicate(format: "NOT( id IN %@)", itemsIds)
        
        do {
            let fetchResults = try context.fetch(fetchRequest) as? [NSManagedObject]
            for nsObj in fetchResults!{
                executeBeforeDelete(nsObj)
                
                if isReadyForDelete(nsObj){
                    context.delete(nsObj)
                }
            }
            CoreDataUtils.sharedInstance.saveContext()
        } catch let fetchError as NSError {
            print("\r\n\r\nERROR - Delete Object: \(fetchError)\r\n\r\n")
        }
    }
    
    static func isReadyForDelete(_ managedObj :NSManagedObject) ->Bool{
        return true
    }
    
    static func executeBeforeDelete(_ managedObj :NSManagedObject){}
    
    static func populateItem(record: [String : Any], item:NSManagedObject, mapping: [String:String]?) -> NSManagedObject
    {
        let attributes = item.entity.attributesByName
        
        for (name, attDescription) in attributes {
            if let newRecord = getRecord(record: record, attribute: name, attributeMapping: mapping?[name]){
                CoreDataUtils.ParseValue(attrDescription: attDescription, obj: item, value: newRecord)
            }
        }
        return item
    }
    
    
    static func getObjectBySFID(context: NSManagedObjectContext, id: String?) -> NSManagedObject?
    {
        var result: NSManagedObject?
        if id != nil && id != ""{
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
            fetchRequest.predicate = NSPredicate(format: "id CONTAINS [c] %@", id!)
            
            do {
                let fetchResults = try context.fetch(fetchRequest) as? [NSManagedObject]
                if (fetchResults?.count)! > 0 {
                    result = fetchResults![0]
                }
            } catch let err as NSError {
                print("\r\n\r\nERROR - Execute Fetch: \(err)\r\n\r\n")
                result = nil
            }
        }
        return result
    }
    
    
    static func getById(context: NSManagedObjectContext, id: NSManagedObjectID) -> NSManagedObject? {
        return context.object(with: id)
    }
    
    
    static func delete(context: NSManagedObjectContext, id: NSManagedObjectID)
    {
        if let itemToDelete = getById(context: context, id: id){
            context.delete(itemToDelete)
        }
    }
    
    static func delete(id: String?){
        guard let id = id else{
            return
        }
        let context = CoreDataUtils.sharedInstance.context
        
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = NSPredicate(format: "id CONTAINS [c] %@", id)
        
        do {
            if let fetchResults = try context?.fetch(fetchRequest) as? [NSManagedObject]{
                for fetchItem in fetchResults{
                    context?.delete(fetchItem)
                }
            }
        } catch let err as NSError {
            print("\r\n\r\nERROR - Execute Fetch: \(err)\r\n\r\n")
        }
    }
    
    static func deleteItem(item: NSManagedObject){
        let context = CoreDataUtils.sharedInstance.context
        context?.delete(item)
        CoreDataUtils.sharedInstance.saveContext(context: context!, withParent: true)
    }
    
    static func refreshItem(item: NSManagedObject) {
        let context = CoreDataUtils.sharedInstance.context
        context?.refresh(item, mergeChanges: false)
    }
    
    static func performFetchRequest(context: NSManagedObjectContext, withPredicate: NSPredicate?, andSortBy: [NSSortDescriptor]?) -> [NSManagedObject]?
    {
        var result: [NSManagedObject]?
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        fetchRequest.predicate = withPredicate
        fetchRequest.sortDescriptors = andSortBy
        do {
            let fetchResults = try context.fetch(fetchRequest) as? [NSManagedObject]
            result = fetchResults
        } catch let err as NSError {
            print("\r\n\r\nERROR - Execute Fetch: \(err)\r\n\r\n")
            result = nil
        }
        return result
    }
    
    
    func ParseValueToValidJSONValue(attrDescription: NSAttributeDescription, value: AnyObject?)
        -> String?
    {
        
        if let val = value {
            switch attrDescription.attributeType {
            case .booleanAttributeType:
                return "\((val as! Bool) ? "true" : "false")"
            case .dateAttributeType:
                return nil
            default:
                return "\(val)"
            }
        }
        
        return nil
    }
    
    
    static func getRecord(record: [String : Any], attribute:String, attributeMapping:String?)-> Any?
    {
        var result = record
        if let sfPath = attributeMapping?.components(separatedBy: "."){
            if(sfPath != nil){
                for pathItem in sfPath{
                    if (result[pathItem] as? [String : AnyObject]) != nil{
                        result = (result[pathItem] as? [String : AnyObject])!
                    }
                    else if let currVal = result[pathItem]{
                        return currVal
                    }
                }
            }
        }else{
            if let currVal = result[attribute]{
                return currVal
            }
        }
        
        return nil
    }
    
    static func getRecords(by ids:[String]? = nil, sortDescriptors:[(key:String, ascending:Bool)]? = nil) -> [NSManagedObject]?{
        let context = CoreDataUtils.sharedInstance.context
        let templatetFetch = NSFetchRequest<NSFetchRequestResult>(entityName: entityName)
        
        if let ids = ids{
            let pred = NSPredicate(format: "id IN %@",ids)
            templatetFetch.predicate = pred
        }
        
        if let sortDescriptors = sortDescriptors{
            var sortBy = [NSSortDescriptor]()
            
            for sortItem in sortDescriptors{
                sortBy.append(NSSortDescriptor(key: sortItem.key, ascending: sortItem.ascending))
            }
            templatetFetch.sortDescriptors = sortBy
        }
        
        do {
            let fetchedRecords = try context?.fetch(templatetFetch) as! [NSManagedObject]
            if fetchedRecords.count > 0{
                return fetchedRecords
            }
            return nil
        } catch {
            fatalError("Failed to fetch record: \(error)")
        }
        return nil
    }
    
    static func getRecord(by id:String, sortDescriptors:[(key:String, ascending:Bool)]? = nil) -> NSManagedObject?{
        if let items = getRecords(by:[id], sortDescriptors: sortDescriptors){
            return items[0]
        }
        return nil
    }
}
