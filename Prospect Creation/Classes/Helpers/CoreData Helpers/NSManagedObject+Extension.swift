//
//  NSManagedObject+Extension.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 25/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import CoreData
import UIKit

extension NSManagedObject{
    func updateManagedObject<T :NSManagedObject>(map: [String: AnyObject])->T{
        for (key,value) in map{
            self.setValue(value, forKey: key)
        }
        return self as! T
    }
}
