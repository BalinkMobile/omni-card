//
//  UIView + CustomAlert.swift
//  Berluti
//
//  Created by elie buff on 15/01/2018.
//  Copyright © 2018 elie buff. All rights reserved.
//

import UIKit

extension UIViewController {
    
    /*func customConfirmMessage(messageLabel :UILabel, iconName :String, onConfirm: (() -> ())? = nil){
        
        let _ = CustomPopup(subjectText: "Are you sure?", subtitleLbl: messageLabel, image: UIImage(named: iconName), buttonsArray: [
            (title:"CONFIRM",backgroundColor: #colorLiteral(red: 0.3843137255, green: 0.8509803922, blue: 0.3882352941, alpha: 1))
        ]) { (tag) in
            if tag == 0{
                if let onConfirm = onConfirm{
                    onConfirm()
                }
            }
        }
    }
    
    func customDoneMessage(messageLabel :UILabel, iconName :String, onConfirm: (() -> ())? = nil){
        
        let _ = CustomPopup(subjectText: "Done", subtitleLbl: messageLabel, image: UIImage(named: iconName), buttonsArray: [
            (title:"OK",backgroundColor: #colorLiteral(red: 0.3843137255, green: 0.8509803922, blue: 0.3882352941, alpha: 1))
        ]) { (tag) in
            if tag == 0{
                if let onConfirm = onConfirm{
                    onConfirm()
                }
            }
        }
    }*/
    
    func customCancelConfirmMessage(title :String, message :String, iconName :String, onConfirm: (() -> ())? = nil){
        let _ = CustomPopup(subject: title, subtitle: message, image: UIImage(named: iconName), buttonsArray: [
            (title:"Cancel",textColor: #colorLiteral(red: 0.6431372549, green: 0.6431372549, blue: 0.6666666667, alpha: 1)),
            (title:"OK",textColor: #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1))
        ]) { (tag) in
            if tag == 1{
                if let onConfirm = onConfirm{
                    onConfirm()
                }
            }
        }
    }
    
    func customErrorMessage(message :String, iconName :String, onConfirm: (() -> ())? = nil){
        let _ = CustomPopup(subject: "Something went wrong", subtitle: message, image: UIImage(named: iconName), buttonsArray: [(title:"OK",textColor: #colorLiteral(red: 0.9764705882, green: 0.2352941176, blue: 0.1607843137, alpha: 1))],onComplete: nil)
    }
}




