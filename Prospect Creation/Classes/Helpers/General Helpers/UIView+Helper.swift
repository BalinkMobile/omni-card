//
//  UIView+Helper.swift
//  Berluti
//
//  Created by Shlomo Ariel on 28/01/2018.
//  Copyright © 2018 elie buff. All rights reserved.
//

import UIKit

extension UIView {
    func applyGradient(first:UIColor, second:UIColor, start:CGPoint, end:CGPoint) {
        let gradientLayer:CAGradientLayer = CAGradientLayer()
        gradientLayer.frame.size = self.frame.size
        gradientLayer.colors = [first.cgColor,second.withAlphaComponent(1).cgColor]
        gradientLayer.startPoint = start
        gradientLayer.endPoint = end
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
    
    func fadeIn(_ alpha: CGFloat = 1.0, duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = alpha
        }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.3, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIViewAnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    func openBottomSlider(parentView:UIView){
        let topGap = parentView.bounds.size.height - self.bounds.size.height
        let finalY = parentView.bounds.size.height/2 + topGap/2
        UIView.animate(withDuration: Double(0.3),
                       delay: 0,
                       options: UIViewAnimationOptions.curveEaseOut,
                       animations: {self.center = CGPoint(x: self.center.x, y: finalY) },
                       completion: nil)
    }
    func sliderFromBottomDrag(sender:UIPanGestureRecognizer, parentView: UIView){
        parentView.bringSubview(toFront: self)
        let translation = sender.translation(in: parentView)
        let topGap = parentView.bounds.size.height - self.bounds.size.height
        let top = (self.center.y - self.bounds.size.height/2) - topGap
        if(top + translation.y >= 0 && top + translation.y < self.bounds.size.height){
            self.center = CGPoint(x: self.center.x, y: self.center.y + translation.y)
        }
        if sender.state == UIGestureRecognizerState.ended {
            let velocity = sender.velocity(in: parentView)
            var finalY = parentView.bounds.size.height/2 + topGap/2
            if velocity.y>0{
                finalY = (parentView.bounds.size.height + self.bounds.size.height/2)
            }
            let finalPoint = CGPoint(x:sender.view!.center.x, y:CGFloat(finalY))
            
            UIView.animate(withDuration: Double(0.3),delay: 0,
                           options: UIViewAnimationOptions.curveEaseOut,
                           animations: {sender.view!.center = finalPoint },
                           completion: nil)
        }
        sender.setTranslation(CGPoint.zero, in: parentView)
    }
}
