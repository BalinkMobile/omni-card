//
//  UIView + FlipAnimation.swift
//  Prospect Creation
//
//  Created by elie buff on 14/10/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

extension UIView {
    func startAnimatingFlip(with duration: TimeInterval = 200.0, scaleX: Int = 1) {
        UIView.animate(withDuration: duration, delay: 0.0, options: .curveLinear, animations: {
                self.transform = CGAffineTransform(scaleX: CGFloat(scaleX), y: 1)//targetView.transform.rotated(by: CGFloat(Double.pi))
            }) { finished in
                self.startAnimatingFlip(with: duration, scaleX : scaleX * -1)
            }
        }
}
