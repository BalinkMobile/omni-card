//
//  Int+currency.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation

extension Int{
    func toCurrency() -> String {
        var currencyCode = "EUR"
       
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = currencyCode
        //formatter.locale = Locale(identifier: localeIdentifier)
        formatter.maximumFractionDigits = 0
        return formatter.string(from: NSNumber(value: self)) ?? "\(self)"
    }
    
    func toCurrency(currencyCode:String) -> String {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        formatter.currencyCode = currencyCode
        //formatter.locale = Locale(identifier: localeIdentifier)
        formatter.maximumFractionDigits = 0
        return formatter.string(from: NSNumber(value: self)) ?? "\(self)"
    }
    
    func toCurrencyWithK() -> String{
        return (self >= 10000 ? "\((self / 1000).toCurrency())K" : "\(self.toCurrency())")
    }
}

