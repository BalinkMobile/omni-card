//
//  UIScrollView + Scrolling.swift
//  Clientleling Template
//
//  Created by elie buff on 16/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

extension UIScrollView {
    func scrollLeft(by offset:Int, animated: Bool) {
        setContentOffset(CGPoint(x: offset, y: 0), animated: animated)
    }
}
