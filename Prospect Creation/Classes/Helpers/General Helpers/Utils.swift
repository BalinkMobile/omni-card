//
//  Utils.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
import CoreData
import MessageUI
import SystemConfiguration.CaptiveNetwork

class Utils{
    
    static func compareItems(item1: Any?, item2: Any?) ->Bool{
        
        if let i1 = item1 as? String , let i2 = item2 as? String{
            return i1 > i2
        }
        
        if let i1 = item1 as? Int , let i2 = item2 as? Int{
            return i1 > i2
        }
        
        if let i1 = item1 as? Double , let i2 = item2 as? Double{
            return i1 > i2
        }
        
        if let i1 = item1 as? NSDate , let i2 = item2 as? NSDate{
            return i1.compare(i2 as Date) == ComparisonResult.orderedDescending
        }
        return false
    }
    
    static func adjustStartTime(startTime:NSDate?, endTime:NSDate?) ->NSDate?
    {
        if let startTime = startTime, let endTime = endTime{
            if(endTime.compare(startTime as Date) == .orderedAscending){
                return  endTime
            }
        }
        return startTime
    }
    
    
    static func getEntityFields(entity:NSManagedObject)->[String]{
        let keys = entity.entity.attributesByName
        var fields:[String]!
        for key in keys{
            fields.append(key.key)
        }
        return fields
    }
    
    
    static func removeMapSubLevels( mapping:[String:String])
        -> [String:String]
    {
        var mapping = mapping
        for map in mapping{
            if map.value.contains("."){
                mapping.removeValue(forKey: map.key)
            }
        }
        return mapping
    }
    
    static func convertManagedObjectToSFDictionary(item:NSManagedObject) -> [String: AnyObject?]
    {
        var convertedDictionary = [String:AnyObject]() // Holds return dictionary
        
        let attributes = item.entity.attributesByName // Holds the entities attributes
        
        let dictionaryWithValues = item.dictionaryWithValues(forKeys: Array(attributes.keys))
        
        for item in dictionaryWithValues{
            switch attributes[item.key]!.attributeType {
            case .dateAttributeType:
                if !(item.value is NSNull){
                    convertedDictionary[item.key] = (item.value as! Date).toString(format: .sfDateTime) as AnyObject?
                }
            default:
                if !(item.value is NSNull){
                    convertedDictionary[item.key] = item.value as AnyObject
                }
            }
        }
        return convertedDictionary
    }
    
    static func convertMapObjToSFDictionary(item:[String: Any?]) -> [String: AnyObject?]
    {
        var convertedDictionary = [String:AnyObject]() // Holds return dictionary
        
        for key in item.keys{
            if let date = item[key] as? Date {
                convertedDictionary[key] = date.toString(format: .sfDateTime) as AnyObject?
            }
            else {
                convertedDictionary[key] = item[key] as AnyObject
            }
        }
        
        return convertedDictionary
    }
    
    
    //MARK: Plist functions
    static func getPlistValue(for key:String)->Any?{
        guard  let infoPlist =  Bundle.main.infoDictionary else{
            return nil;
        }
        return infoPlist[key]
    }
    
    
    static func JSONParseDict(jsonString:String) -> Dictionary<String, AnyObject> {
        do {
            if let data: NSData = jsonString.data(using: String.Encoding.utf8) as NSData?{
                let jsonObj = try JSONSerialization.jsonObject(with: data as Data, options: []) as? Dictionary<String, AnyObject>
                return jsonObj!
            }
            return [String: AnyObject]()
        }
        catch _ as NSError {
            return [String: AnyObject]()
        }
    }
    
    
    static func JSONParseArray(jsonString:String) -> [Dictionary<String,AnyObject>] {
        do {
            if let data: NSData = jsonString.data(using: String.Encoding.utf8) as NSData?{
                let jsonObj = try JSONSerialization.jsonObject(with: data as Data, options: []) as? [Dictionary<String,AnyObject>]
                return jsonObj!
            }
            return [Dictionary<String,AnyObject>]()
        }
        catch _ as NSError {
            return [Dictionary<String,AnyObject>]()
        }
    }
    
    static func printJson(_ data : [String: AnyObject]){
        do {
            let theJSONData = try JSONSerialization.data(withJSONObject: data , options: JSONSerialization.WritingOptions(rawValue: 0))
            let theJSONText = NSString(data: theJSONData, encoding: String.Encoding.ascii.rawValue)
            print("JSON string = \(theJSONText!)")
        }
        catch (let error) {
            print (error)
        }
    }
    
    
    
    static func getValueFromRecord(record: [String : AnyObject], mappingValue : String?)-> AnyObject?
    {
        
        let sfPath = mappingValue?.components(separatedBy: ".")
        var result = record
        if(sfPath != nil){
            for pathItem in sfPath!{
                if (result[pathItem] as? [String : AnyObject]) != nil{
                    result = (result[pathItem] as? [String : AnyObject])!
                }
                else if let currVal = result[pathItem]{
                    return currVal
                }
            }
        }
        return nil
    }
    
    
    static func arrayOfCommonElements <T, U> (lhs: T, rhs: U) -> [T.Iterator.Element] where T: Sequence, U: Sequence, T.Iterator.Element: Equatable, T.Iterator.Element == U.Iterator.Element {
        var returnArray:[T.Iterator.Element] = []
        for lhsItem in lhs {
            for rhsItem in rhs {
                if lhsItem == rhsItem {
                    returnArray.append(lhsItem)
                }
            }
        }
        return returnArray
    }
    
    
    static func fetchSSIDInfo() ->  String? {
        if let interfaces = CNCopySupportedInterfaces() {
            for i in 0..<CFArrayGetCount(interfaces){
                let interfaceName: UnsafeRawPointer = CFArrayGetValueAtIndex(interfaces, i)
                let rec = unsafeBitCast(interfaceName, to: AnyObject.self)
                let unsafeInterfaceData = CNCopyCurrentNetworkInfo("\(rec)" as CFString)
                
                if let unsafeInterfaceData = unsafeInterfaceData as? Dictionary<AnyHashable, Any> {
                    return unsafeInterfaceData["SSID"] as? String
                }
            }
        }
        return nil
    }
    
    static func SSIDAlertMessage()-> (title :String, description :String){
        return (title:"LV Network", description:"You are not connected to LV network")
    }
    
    static func delay(_ delay:Double, closure:@escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }
    
    
    
    static func listToSOQLString(list:[String]) -> String {
        let localList = list.filter { $0 != ""}
        
        let SOOQListString = localList.reduce(""){ (prev, item) -> String in
            var prefix = ","
            if prev == ""{
                prefix = ""
            }
            
            return "\(prev)\(prefix) '\(item)'"
        }
        if SOOQListString == ""{
            return "'null'"
        }
        return SOOQListString
    }
    
    static func listToString(list:[String]) -> String {
        let localList = list.filter { $0 != ""}
        
        let ListString = localList.reduce(""){ (prev, item) -> String in
            var prefix = ";"
            if prev == ""{
                prefix = ""
            }
            
            return "\(prev)\(prefix)\(item)"
        }
        return ListString
    }
    
    /// OPTIONAL Added method to adjust lock and rotate to the desired orientation
    
    static func getData(name: String) -> Data? {
        guard let bundleURL = Bundle.main
            .url(forResource: name, withExtension: "gif") else {
                return nil
        }
        
        // Validate data
        guard let data = try? Data(contentsOf: bundleURL) else {
            return nil
        }
        
        return data
    }
    
    static func toJson(data: [String: Any]) ->String{
        let jsonData = try? JSONSerialization.data(withJSONObject: data, options: [])
        return String(data: jsonData!, encoding: .utf8) ?? ""
    }
    
    static func fromJson(stringData: String) ->[String: Any]{
        let jsonData = stringData.data(using: .utf8)
        return try! JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! [String: Any]
    }
}
