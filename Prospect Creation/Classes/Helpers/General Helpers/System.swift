//
//  System.swift
//  Clientleling Template
//
//  Created by elie buff on 02/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation
import UIKit

struct System {
    static func clearNavigationBar(forBar navBar: UINavigationBar) {
        navBar.setBackgroundImage(UIImage(), for: .default)
        navBar.shadowImage = UIImage()
        navBar.isTranslucent = true
    }
}
