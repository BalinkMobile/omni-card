//
//  Calendar + UTC.swift
//  Clientleling Template
//
//  Created by elie buff on 16/09/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation

extension Calendar {
    
    static let utc: Calendar  = {
        var calendar = Calendar.current
        calendar.timeZone = TimeZone(identifier: "UTC")!
        return calendar
    }()
    
    static let localTime: Calendar  = {
        var calendar = Calendar.current
        calendar.timeZone = .current
        return calendar
    }()
    
}
