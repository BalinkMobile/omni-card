//
//  String+Helper.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
extension String {
    var unCapitalizeFirstLetter: String {
        get {
            let first = String(characters.prefix(1)).lowercased()
            let other = String(characters.dropFirst())
            return first + other
        }
    }
    var capitalizeFirstLetter: String {
        get {
            let first = String(characters.prefix(1)).uppercased()
            let other = String(characters.dropFirst())
            return first + other
        }
    }
    
    var length: Int {
        get {
            return characters.count
        }
    }
    
    func toDate(format: DateFormat, timezone: String? = nil) -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        if let timezone = timezone {
            dateFormatter.timeZone = NSTimeZone(name: timezone) as TimeZone!
        }
        return dateFormatter.date(from: self as String)
    }
    
    func isEmail() -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: "^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]+$", options: NSRegularExpression.Options.caseInsensitive)
            return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.characters.count)) != nil
        } catch { return false }
    }
    
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
