//
//  Array+ToDict.swift
//  IOS Template
//
//  Created by elie buff on 05/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

extension Array {
    public func toDictionary<Key: Hashable>(with selectKey: (Element) -> Key) -> [Key:Element] {
        var dict = [Key:Element]()
        for element in self {
            dict[selectKey(element)] = element
        }
        return dict
    }
}
