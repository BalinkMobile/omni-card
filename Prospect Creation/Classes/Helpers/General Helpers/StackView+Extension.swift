//
//  StackView+Extension.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 13/08/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import Foundation
import UIKit

extension UIStackView{
    func empty(){
        for view in self.arrangedSubviews{
            self.removeArrangedSubview(view)
            view.removeFromSuperview()
        }
    }
}
