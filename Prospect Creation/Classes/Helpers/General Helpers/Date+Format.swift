//
//  Date+Format.swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import Foundation
extension Date {
    
    func toString(format: DateFormat, addTimeZone: Bool = false) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format.rawValue
        if addTimeZone {
            dateFormatter.timeZone = NSTimeZone.init(abbreviation: "GMT") as TimeZone!
        }
        return dateFormatter.string(from: self as Date)
    }
    
    static func from(day:String?, month:String?, year:String?) -> Date?{
        if let day = day, let month = month, let year = year, !day.isEmpty, !month.isEmpty, !year.isEmpty{
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "MM-dd-yyyy"
            return dateFormatter.date(from: "\(month)-\(day)-\(year)")!
        }
        else{
            return nil
        }
    }
    
    static func getMonday(_ myDate: Date? = nil) -> Date {
        let today = myDate != nil ? myDate : Date()
        let cal = Calendar.current
        var comps = cal.dateComponents([.weekOfYear, .yearForWeekOfYear], from: today!)
        comps.weekday = 2 // Monday
        let mondayInWeek = cal.date(from: comps)!
        return mondayInWeek
    }
   
    
    static func getWeekdays(by date: Date? = nil) -> [Date]{
        let today = getMonday(date)
        var days = [Date]()
        for i in 0 ... 6 {
            let day = Calendar.current.date(byAdding: .day, value: i, to: today)!
            days.append(day)
        }
        return(days)
    }
    
    func addDays(numOfDay: Int) -> Date{
        return Calendar.current.date(byAdding: .day, value: numOfDay, to: self as Date) as! Date
    }
    
    func addHours(numOfHours: Int) -> Date{
        return Calendar.current.date(byAdding: .hour, value: numOfHours, to: self as Date) as! Date
    }
    
    func getDateComponents() -> (year:String,month:String,day:String)
    {
        let calendar = Calendar.current
        
        let year = calendar.component(.year, from: self as Date)
        let month = calendar.component(.month, from: self as Date)
        let day = calendar.component(.day, from: self as Date)
        
        return(year:String(year),month:String(month),day:String(day))
    }
    
    func shortDateTime() -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .short
        dateFormatter.timeStyle = .short
        return dateFormatter.string(from: self as Date)
    }
    
    func toAMPMTime() -> String {
        let dateFormatter = DateFormatter()
        
        dateFormatter.amSymbol = "am"
        dateFormatter.pmSymbol = "pm"
        dateFormatter.dateFormat = "h:mm a"
        return dateFormatter.string(from: self as Date)
    }
    
    func getDuration(from startDate: Date) ->DateComponents{
        let calendar = NSCalendar.current
        return calendar.dateComponents([.hour,.minute], from: startDate, to: self)
    }
    
    func startOfDay() -> Date {
        return Calendar.utc.startOfDay(for: self)
    }
    
    func getDaysDifference(date: Date) -> Int {
        let components = Calendar.current.dateComponents([.day], from: date.startOfDay(), to: self.startOfDay())
        return components.day!
    }
    
    func getHoursDifference(date: Date) -> Int {
        let components = Calendar.current.dateComponents([.hour], from: date, to: self)
        return components.hour!
    }
    
    func inPast() -> Bool{
        return getDaysDifference(date: Date()) < 0
    }
    
    func inFutur() -> Bool{
        return getDaysDifference(date: Date()) > 0
    }
    
    func todayOrFutur() -> Bool{
        return getDaysDifference(date: Date()) >= 0
    }
    
    func isToday() -> Bool{
        return Calendar.current.isDate(self, inSameDayAs:Date())
    }
    
    func isInLastNDays(ndays:Int)->Bool{
        let dateReference = Date().addDays(numOfDay: -ndays)
        return self.compare(dateReference) == ComparisonResult.orderedDescending
    }
    
    func getWeekday() ->Int{
        return Calendar.current.component(.weekday, from: self)
    }
}
