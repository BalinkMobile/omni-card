//
//  CellConfigurator.swift
//  Clientleling Template
//
//  Created by Jeremy Martiano on 31/07/2018.
//  Copyright © 2018 Balink. All rights reserved.
//

import UIKit

protocol ConfigurableCell {
    associatedtype T
    func configure(data: T)
}

protocol CellConfigurator {
    static var reuseId: String { get }
    var isSelectedCell: Bool { get set }
    
    func configure(cell: UIView)
}

class TableCellConfigurator<CellType: ConfigurableCell, T>: CellConfigurator where CellType.T == T, CellType: UITableViewCell {
    
    let item: T
    var isSelectedCell: Bool
    
    static var reuseId: String { return String(describing: CellType.self) }
    
    init(item: T, isSelected: Bool = false) {
        self.item = item
        self.isSelectedCell = isSelected
    }
    
    func configure(cell: UIView) {
        (cell as! CellType).configure(data: self.item)
    }
}

class CollectionCellConfigurator<CellType: ConfigurableCell, T>: CellConfigurator where CellType.T == T, CellType: UICollectionViewCell {
    
    let item: T
    var isSelectedCell: Bool
    
    static var reuseId: String { return String(describing: CellType.self) }
    
    required init(item: T, isSelected: Bool = false) {
        self.item = item
        self.isSelectedCell = isSelected
    }
    
    func configure(cell: UIView) {
        (cell as! CellType).configure(data: item)
    }
}
