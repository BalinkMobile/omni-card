//
//  UITextField+attributed..swift
//  Berluti
//
//  Created by elie buff on 31/10/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//

import UIKit

extension UITextField
{
    func setColorInRange(_ color:UIColor, ranges:NSArray)
    {
        let myString = NSMutableAttributedString(attributedString: self.attributedText!)
        for value in ranges
        {
            myString.addAttribute(NSAttributedStringKey.foregroundColor, value: color, range: (value as AnyObject).rangeValue)
        }
        self.attributedText = myString
    }
    
    enum Direction {
        case Left
        case Right
    }
    
    func withImage(image: UIImage, direction: Direction = .Left, imgSize : (width: CGFloat, height:CGFloat)? = nil, colorSeparator: UIColor? = nil , colorBorder: UIColor? = nil){
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: (imgSize?.width ?? 15) * 2, height: self.frame.height))
        mainView.layer.cornerRadius = 5
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: (imgSize?.width ?? 15) * 2, height: self.frame.height))
        view.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        view.clipsToBounds = true
        view.layer.cornerRadius = 5
        view.layer.borderWidth = colorBorder != nil ? CGFloat(0.5) : 0
        view.layer.borderColor = colorBorder?.cgColor
        mainView.addSubview(view)
        
        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleAspectFit
        imageView.frame = CGRect(x: (direction == .Left ? 10.0 : 0), y: (self.frame.height  -  (imgSize?.height ?? 15.0)) / 2, width: imgSize?.width ?? 15.0, height: imgSize?.height ?? 15.0)
        view.addSubview(imageView)
        
        if let colorSeparator = colorSeparator{
            let seperatorView = UIView()
            seperatorView.backgroundColor = colorSeparator
            mainView.addSubview(seperatorView)
            
            if(Direction.Left == direction){ // image left
                seperatorView.frame = CGRect(x: (imgSize?.width ?? 15) * 2, y: 0, width: 5, height: self.frame.height)
            } else { // image right
                seperatorView.frame = CGRect(x: 0, y: 0, width: 5, height: self.frame.height)
            }
        }
        
        if(Direction.Left == direction){ // image left
            self.leftViewMode = .always
            self.leftView = mainView
        } else { // image right
            self.rightViewMode = .always
            self.rightView = mainView
        }
        
        self.layer.borderColor = colorBorder?.cgColor
        self.layer.borderWidth = colorBorder != nil ? CGFloat(0.5) : 0
        self.layer.cornerRadius = 5
    }
    
    func withImageName(imageName: String, direction: Direction = .Left, imgSize : (width: CGFloat, height:CGFloat)? = nil, colorSeparator: UIColor? = nil , colorBorder: UIColor? = nil){
        if let image = UIImage.init(named: imageName){
            
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
            imageView.contentMode = .center
            imageView.image = image
            
            self.rightViewMode = UITextFieldViewMode.always
            self.rightView = imageView
        }
    }
}
