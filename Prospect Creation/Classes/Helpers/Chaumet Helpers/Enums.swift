//
//  Enums.swift
//  Berluti
//
//  Created by elie buff on 09/11/2017.
//  Copyright © 2017 elie buff. All rights reserved.
//


enum UserDefaultKey : String{
    case userId = "id"
    case lastUpdate = "lastUpdate"
    case lastLogin = "lastLogin"
    case lastClientUpdate = "lastClientUpdate"
    case fullPhotoUrl = "photoUrl"
    case userLanguage = "lang"
    case currency = "DefaultCurrencyIsoCode"
    case firstName = "firstName"
    case lastName = "lastName"
    case email = "email"
    case name = "name"
    case phone = "phone"
    case storeCountry = "storeCountry"
    case storeName = "storeName"
    case storeCode = "storeCode"
    case storeId = "storeId"
    case selectedStore = "selectedStore"
    case selectedLanguage = "selectedLanguage"

    static let allValues = [userId, lastUpdate,lastLogin,fullPhotoUrl,userLanguage,currency,firstName,lastName,email, phone, storeName, storeId, storeCode, selectedStore, selectedStore, selectedLanguage]
}

enum ClearCacheType{
    case Logout
    case Partial
    case NewVersion
    case Complete
    case NewStore
}

enum OutreachStatus: String {
    case Disable = "disable-"
    case Enable = "enable-"
    case Deny = "deny-"
    case Preferred = "preferred-"
    case None = ""
}

enum OutreachType: String {
    case Sms = "Sms"
    case Email = "Email"
    case Chat = "Chat"
    case Call = "Call"
    case Postal = "Postal"
    case Clienteling = "Clienteling"
    case WhatsApp = "WhatsApp"
    case Wechat = "Wechat"
    case Line = "Line"
    case Kakao = "Kakao"
    case Appointment = "Appointment"
    case Task = "Task"
    
    static let barButtons = [Sms, Email, Chat, Call, Postal, Clienteling]
    static let actions = [Sms, Email, Call, Postal, WhatsApp, Wechat, Line, Appointment, Task]
    static let hasMessage = [Sms, Email, WhatsApp]
    static let buttonOptions = [
        Chat: [WhatsApp, Wechat, Line],
        Postal: [Postal],
        Clienteling: [Appointment, Task]
    ]
    static let buttonOptionsTitles = [
        WhatsApp: "Sent WhatsApp",
        Wechat: "Send Wechat",
        Line: "Send Line",
        Postal: "Send Postal Mail",
        Appointment: "Create Appointment",
        Task: "Create Task"
    ]
    static let image = [
        Sms: "Sms",
        Email: "Email",
        Chat: "Chat",
        Call: "Phone",
        Postal: "Postal",
        Clienteling: "Clienteling"
    ]
}

enum ContactType:String
{
    case Call = "Call"
    case Email = "Email"
    case Sms = "Sms"
    case Chat = "Chat"
    case Postal = "Postal"
    case Clienteling = "Clienteling"
}

enum ActivityType:String{
    case Appointment = "Appointment"
    case Task = "Task"
}

enum TimelineType: String {
    case Appointment = "Appointment"
    case Task = "Task"
    case Sale = "Sale"
}

enum DateFormat: String {
    case fullDate = "MMM. dd, yyyy"
    case longDate = "EEEE MMM dd, yyyy"
    case monthDay = "MMM dd"
    case monthYear = "MMM yyyy"
    case shortDate = "dd/MM/yyyy"
    case dateTime = "dd/MM/yyyy HH:mm"
    case sfDate = "yyyy-MM-dd"
    case sfDateTime = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    case sfDateTimeUTC = "yyyy-MM-dd'T'HH:mm:ss.SSS'+0000'"
    case time = "HH':'mm':'ss"
    case day = "d"
    case weekDay = "EEEEE"
    case amPm = "h:mm a"
}

enum ProductCatalogFilterType: String{
    case Collection = "Collection"
    case SubCollection = "SubCollection"
    case Function = "Function"
    case SubFunction = "SubFunction"
    
    static let estheticValues = [Collection, SubCollection, Function, SubFunction]
    static let allValues = [Collection, SubCollection, Function, SubFunction]
}

enum ClientDisplaySegmentControls: String{
    case All = "All"
    case LastContacted = "Recent"
    case Favorites = "Favorites"
}

